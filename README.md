# Development

```
yarn run develop
```

# Preview Deployment

Deployment behind authentication to Gitlab pages happens
via CI on every commit to the main branch, and to next.helfa.org.

To view, visit:

- https://next.helfa.org/

# Use debug proxy

Start the proxy first.

```shell
mitmproxy --listen-host 127.0.0.1 -p 8080
```

Place these values in a `.env.` file.

```conf
# .env file
GLOBAL_AGENT_HTTP_PROXY=http://127.0.0.1:8080
NODE_EXTRA_CA_CERTS=/Users/febeling/.mitmproxy/mitmproxy-ca.pem
```

Add this code to the top of `gatsby-config.js`, after installing packages.

```shell
yarn install dotenv global-agent
```

```js
if (process.env.GLOBAL_AGENT_HTTP_PROXY) {
  require("dotenv").config();
  require("global-agent").bootstrap();
}
```

Then run gatsby. Requests will show in `mitmproxy`.

```shell
yarn run develop
```

There often happens an error when using the proxy though. But only after 10+
successful requests, which confirms that the process is working generally.

```
  Error: The `onCancel` handler was attached after the promise settled.

  - index.js:48 onCancel
    [gatsby-drupal]/[p-cancelable]/index.js:48:12
  ...
```

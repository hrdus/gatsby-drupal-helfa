---
title: H.e.l.f.a. - Das Handbuch
language: de
category: manuals
slug: /manual
---

# H.e.l.f.a. - Das Handbuch

![Das Buch](/images/manual/Book-1920x450.jpg?itok=MqWxX5xC "Das Buch")

**Worum geht es bei H.e.l.f.a. im Allgemeinen?**

Wir leben in einer Welt, in der der andauernde globale Wettbewerb und der Kapitalismus zu Ungerechtigkeit gegenüber einem Teil der Erdbevölkerung führt, die nicht genug zu Essen und zu Trinken hat, während Menschen in der sogenannten westlichen Welt im Überfluss leben.  
Was auf der Strecke bleibt ist das, was wirklich zählt: unsere Menschlichkeit im Miteinander, unsere Familien und Beziehungen, Freundschaften und unsere Gesundheit.

**Daraus resultiert die Frage:** Was in unserem Leben ist wirklich echt und hat Substanz?  
Diese Suche nach Substanz beschäftigt immer mehr Menschen, wodurch eine weltweite Bewegung ausgelöst wurde, die aktiv nach alternativen Lebensmodellen sucht - zum Wohle von Menschen, Tieren und der Natur. Damit die Veränderung und das gegenseitige Helfen und Unterstützen gelingt, müssen wir die derzeitigen Gesellschaftsstrukturen komplett überdenken.

Wir bei H.e.l.f.a. möchten mit diesem Handbuch unseren Lösungsansatz vorstellen, wie ein neues gesellschaftliches Zusammenleben mit Werten wie Mitgefühl, Respekt, Freiwilligkeit, Menschlichkeit, Zusammenhalt, Empathie, Verständnis, Unterstützung, Toleranz, gelebte Liebe und Eigenverantwortung gelingen kann.

Viel Spaß beim Lesen

## Wie es zu der Idee von H.e.l.f.a. kam

In vielen Gesetzen, Verfassungen und Konventionen wird unter anderem der Würde des Menschen und der Selbstbestimmung eine besondere Bedeutung und Wichtigkeit verliehen. In der Realität erleben wir dagegen, wie diese und andere menschliche Werte vermehrt wirtschaftlichen, technologischen und materiellen Interessen untergeordnet werden.

Das Kredo der Wirtschaft: Schneller, Höher, Besser, Weiter (und Schöner) prägt längst auch das soziale Miteinander. Konkurrenzkämpfe, Spaltung und Egomanie sind die Folge. Wir werden ständig mit den oft unrealistischen Leben, Karrieren und Errungenschaften anderer konfrontiert (mein Haus, mein Auto, mein Pferd...). Das daraus resultierende permanente Vergleichen untereinander führt zu Unzufriedenheit, Neid und Missgunst. Infolge dessen optimieren wir uns ständig selbst, unsere Umgebung und auch andere, um besser zu “funktionieren“ und/oder besser zu sein als andere - aber warum oder wofür tun wir das wirklich?

Darüber hinaus führt der andauernde globale Wettbewerb und der Kapitalismus zu einer Ungerechtigkeit gegenüber einem Teil der Erdbevölkerung, die nicht genug zu Essen und zu Trinken hat, während in der sogenannten westlichen Welt Menschen im Überfluss leben.

**Was auf der Strecke bleibt ist das, was wirklich zählt:** unsere Menschlichkeit im Miteinander, unsere Familien und Beziehungen, Freundschaften und unsere Gesundheit.  
Aus diesem Teufelskreis brechen viele erst aus, wenn Körper und Psyche mit ernsthafteren Symptomen darauf aufmerksam machen, dass im Leben etwas schief läuft.

**Daraus resultiert für uns die Frage**: Was in unserem Leben ist wirklich echt und hat Substanz?

Dadurch, dass diese Erkenntnisse immer mehr in das Bewusstsein der Menschen dringen, wurde eine weltweite Bewegung ausgelöst, in der aktiv nach alternativen Lebensmodellen gesucht wird - zum Wohle von Menschen, Tieren und der Natur.  
Für ein gelingendes gemeinsames Helfen und Unterstützen zwischen den Menschen, bedarf es einer neuen Art des menschlichen Miteinanders.

Hierbei geht es nicht darum, das Alte grundsätzlich abzulehnen. Es bedeutet vielmehr Bestehendes daraufhin zu überprüfen, ob es den Menschen, den Tieren und der Natur im besten Sinne dient. Dabei sind wir nicht nur aufgefordert, die existierenden Strukturen, sondern auch das, was wir gelernt und erlebt haben inkl. unserer Gewohnheiten und Glaubenssätze zu hinterfragen.

Es ist eine Bestandsaufnahme dessen, was wahr und echt ist, dessen was von Bedeutung für unser aller Wohl ist und dessen, was wirklich Substanz hat. Diese Essenz soll kontinuierlich und dauerhaft Bestand haben. Das mag am Anfang nicht ganz leicht sein, aber wir sind der festen Überzeugung, dass sich jede Anstrengung dafür lohnt.

In diesem Veränderungsprozess ist es nötig, neu zu denken, neue Wege zu finden und/oder die alten Mechanismen anders zu benutzen. Dabei ist es auch möglich, dass bestehende Strukturen, die gut funktionieren, genauso umgesetzt und genutzt werden, wie bisher.

Uns allen darf bewusst sein, dass es keine Patent-Lösung für die Veränderung gibt. Denn eine Gleichschaltung aller Menschen wollen wir nicht. Jeder Mensch bringt seine Bedürfnisse und Wünsche ein und auch regionale und kulturelle Besonderheiten und Unterschiede finden Beachtung.

Wir werden vieles ausprobieren und das, was nicht funktioniert wieder verwerfen, um so herauszufinden, was für das Miteinander hilfreich ist.

Wir bei H.e.l.f.a. stellen mit diesem Handbuch einen Lösungsansatz vor, wie ein neues gesellschaftliches Zusammenleben mit Werten wie Mitgefühl, Respekt, Freiwilligkeit, Menschlichkeit, Zusammenhalt, Empathie, Verständnis, Unterstützung, Toleranz, gelebte Liebe und Eigenverantwortung funktionieren kann.

Viel Spaß beim Lesen.

## Was ist Helfa?

![2 Schmetterlinge](/images/manual/butterflies-1920x450.jpg?itok=LRYfWIZb "2 Schmetterlinge")

H.e.l.f.a. ist das Akronym für „**H**elfa **e**ngagieren sich **l**iebevoll **f**ür **A**lle“.

H.e.l.fa. ist eine Initiative, die am 6. September 2020 klein begann und seitdem immer weiter wächst.

Die grundsätzliche Frage auf der die bewegende Projekt-Idee H.e.l.f.a. beruht, ist, wie wir **alle** Menschen miteinander verbinden und eine neue Form von Gemeinschaft kreieren können. Die Basis dieser Gemeinschaft gründet dabei auf einem Miteinander aus Menschlichkeit, Vertrauen und Mitgefühl.

Schon bei den indigenen Völkern wurden **alle** Mitglieder der Gemeinschaft respektiert und in die Belange des Stammes einbezogen. Die Stärkeren kümmerten sich um die Schwächeren. Die Ideen und Entscheidungen wurden gemeinsam besprochen und umgesetzt. Jeder brachte sich und seine Fähigkeiten ein, um der Gemeinschaft zu dienen.

Weitere Beispiele aus der jüngeren Geschichte sind der Zusammenhalt der ehemaligen DDR Bürger oder der selbstlose Einsatz von Menschen füreinander bei Naturkatastrophen. Ein anderes Beispiel dafür ist Rojava: In diesem Gebiet in Syrien leben Kurden, Araber, Turkmen, Armenier und Tschetschen friedlich zusammen und alle praktizieren ihren Glauben ohne Diskriminierung zu erfahren. Ein friedliches Miteinander selbst unter verschiedenen Glaubensrichtungen ist also möglich, wenn wir Menschen es zulassen können und wollen.

Ebenso sind die „klassische“ Nachbarschaftshilfe, Schenkungsgesellschaften, Ubuntu oder gemeinnützige Vereine lebendige Beispiele für ein menschliches Miteinander. In all diesen Gemeinschaften waren und sind Hilfe und Unterstützung in jeglicher Form gängige Praxis.

**Die Vision, die wir mit H.e.l.f.a. umsetzen wollen** ist demnach schon in unterschiedlichster Art und Weise erprobt worden.

All dem zugrunde liegt die seit Urzeiten tief in uns Menschen verankerte Sehnsucht nach echter Zugehörigkeit zu einer Gemeinschaft. Diesem menschlichen Grundbedürfnis soll auch mit dem H.e.l.f.a.-Projekt wieder mehr Ausdruck und Bedeutung verliehen werden. Mit einem gelebten Zusammensein in Verbundenheit mit unseren Mitmenschen wollen wir der Vereinsamung, Trennung, Anonymisierung und Diskriminierung entgegenwirken und so aktiv an einer schöneren und lebenswerteren Zukunft arbeiten. Alles, was ein jeder dazu braucht ist eine Entscheidung und die Bereitschaft wieder mehr Verantwortung für sein und das Leben in der Gemeinschaft zu übernehmen.

**Das Neue am H.e.l.f.a. Ansatz ist,** dass wir reale und praktische Hilfe mit der globalen Vernetzung durch das Internet anstreben. Denn nur wenn alle Menschen auf der ganzen Welt zusammenhalten, gelingt es, das alte System durch Menschlichkeit zu ersetzen.

Unsere Vision ist also, alte und bewährte Konzepte mit neuen Ideen zu vereinen und diese zusammen mit vielen Helfas zu neuem Leben zu erwecken.

Des Weiteren steht H.e.l.f.a. für eine schenkende Gesellschaft, die im Idealfall ohne Geld funktioniert. Dabei greifen wir auf viele erprobte und bewährte Methoden und Lebensweisen zurück. Hierzu zählen unter anderem das Verhalten alter Stämme, die im Einklang mit der Natur leben, psychologisches Wissen und Spiritualität sowie der OpenSource-/ OpenAccess-Gedanke, die sich für transparente und frei verfügbare Informationen in der Computerwelt durchgesetzt haben.

**H.e.l.f.a. setzt sich aktiv mit der Frage auseinander, in welcher Welt- und Wertegemeinschaft wir leben wollen.**

Für manche mag diese Art zu Denken und zu Handeln am Anfang noch ungewohnt und neu sein - der Mensch ist nun einmal ein Gewohnheitstier. Doch lasst uns **JETZT** damit starten, gemeinsam eine neue Welt aufzubauen, in der wir Menschlichkeit leben.

Wir laden Dich dazu ein, mit uns zusammen eine neue Wertegemeinschaft zu erschaffen, in der wir uns alle wohlfühlen.

Damit wir bei aller Unterschiedlichkeit eine gemeinsame Basis haben, gibt es [vier Grundregeln](/node/73), auf denen H.e.l.f.a. aufbaut.

###### Political view

Politik und politische Inhalte

Immer wieder wird die Frage gestellt, wie wir mit politischen Themen umgehen, welche Position wir einnehmen.

Nach unserem Verständnis sind politische Themen, aber auch Parteien oder andere politisch orientierte Institutionen eher daran interessiert, rein ideologisch und machtorientiert zu handeln, so dass dies die Bevölkerung verunsichert und spaltet. Einen Menschen, der anderen Menschen nicht weh tut, zu etwas zu zwingen, was ihm nicht gefällt, geht beispielsweise in Richtung Diktatur und Folter und damit Unmenschlichkeit.

Deshalb wird sich H.e.l.f.a. nicht offiziell zu brisanten Themen äußern.

Natürlich werden die Themen diskutiert und damit Ideen, Gedanken und Meinungen ausgetauscht. Denn es ist wichtig, dass alle relevanten Themen diskutiert werden und dass sich jeder seine Meinung bilden kann.

In diesem Zusammenhang sind uns Wertschätzung, Respekt und Akzeptanz von unterschiedlichen Ansichten wichtig. Denn wenn nicht mehr über Inhalte geredet wird, sondern ideologisch festgelegt wird, was gedacht oder gesagt werden darf, dann schränkt das die Lösungsfindung ein. Wir von H.e.l.f.a. wollen Projekte initiieren, Leben gestalten und kreativ denken und handeln.

H.e.l.f.a. versucht zu verbinden und ist tolerant gegenüber allen Menschen und unterschiedlichen Meinungen.

Warum?

Wir müssen anders denken, um die Probleme der Gesellschaft auf dieser Erde zu überwinden. Unterschiedliche Gedanken, Erfahrungen und Herangehensweisen an Themen und Projekte sind bereichernd. Wenn wir alle gleich denken würden, würden wir alle an dem gleichen Problem scheitern. Wir wollen, dass Menschen anders denken. Wir wollen, dass Probleme aus verschiedenen Blickwinkeln betrachtet werden. Nur so können wir uns ein vollständigeres Bild von der Situation machen. Das versuchen wir auch in unserem Handbuch zu reflektieren.

Nur wenn Lösungen im Geiste der Mitmenschlichkeit umgesetzt werden und nicht gegen die Menschenrechte oder andere Persönlichkeitsrechte verstoßen, werden sie langfristig erfolgreich sein.

Wir sind überzeugt, dass eine freie Entfaltung der Menschen dazu führt, dass sie über sich hinauswachsen. Wir bevorzugen die intrinsische Motivation, mit der jedes Kind auf der Welt geboren wird. Diese basiert auf Neugier, Lust, Liebe und der Menschlichkeit, anderen zu helfen.

Die 10 Prinzipien zeigen, dass es uns wichtig ist, dass wir alle miteinander und nicht gegeneinander arbeiten - lasst uns also eine Welt schaffen, die uns alle verbindet - nicht nur die Menschen, sondern alle Lebewesen.

Aber um es noch einmal ganz deutlich zu machen:  
Wir sind NICHT politisch und wir sind gegen jede Art von Diskriminierung, Segregation und Spaltung. Für uns findet Ausgrenzung nur dann statt, wenn sie andere Menschen in Gefahr bringt oder sie anderweitig angreift, und das ist die letzte Möglichkeit, die in Betracht gezogen werden sollte. Davor sollten wir unsere Achtsamkeit und Kommunikationsfähigkeit nutzen, um mögliche Diskriminierungen etc. frühzeitig zu verhindern.

## Die vier H.e.l.f.a. Grundregeln - 4 plus 1

![4 Blatt Klee](/images/manual/four-leaf-clover-1920x450.jpg?itok=L2dpSeWE "4 Blatt Klee")

### Die vier H.e.l.f.a. Grundregeln:

In einer Gemeinschaft, in der sich auch zukünftige Generationen ein sicheres, lebenswertes und substanzielles Leben aufbauen können, braucht es verlässliche Werte und Grundsätze.

H.e.l.f.a. bietet Mindeststandards an, die dabei helfen, Gemeinschaften zu organisieren, sodass die Menschen, mit all ihren Unterschieden, gut miteinander zusammenleben, umgehen und arbeiten können.

**Dieses Miteinander ist in den folgenden 4 plus 1 Grundregeln beschrieben:**

1. Das Schenken - Der Schenkungsgedanke beschreibt unsere Haltung , wie wir eine neue Art des Miteinanders schaffen. (Detailbeschreibung [hier](/node/6))

2. Einander Kennenlernen - Das Kennenlernen dient dem Wieder-Annähern von uns allen, sodass wir einander wieder vertrauen. Vertrauen ist die Basis für das Gelingen des Schenkungsgedankens. (Detailbeschreibung [hier](/node/10))

3. Moderatoren (Ansprechpartner) – Die Moderatoren sind da, um auch überregional oder sogar international den Kontakt halten zu können, obwohl wir uns nicht persönlich kennen. Sie bauen Gruppen auf, unterstützen und stärken diese. (Detailbeschreibung hier. (Detailbeschreibung [hier](/node/11))

4. Eigene Regeln der Gruppen – Die eigenen Regeln sollen gruppenspezifische, regionale und kulturelle Besonderheiten respektieren und berücksichtigen. So können alle Gruppen, zusätzlich zu den von H.e.l.f.a. gegebenen vier Grundregeln, eigene Grundsätze für ihre Belange definieren. Dies soll die Individualität und Freiheit im Umgang miteinander jeder einzelnen Gruppe stützen. (Detailbeschreibung [hier](/node/12))

Innerhalb dieser vier Grundregeln können alle Menschen ihre Fähigkeiten einbringen und sich ihrem eigenen Tempo entsprechend entwickeln.

Und die Plus 1 Regel kommt bei uns immer wieder vor - denn die Plus 1 Regel bedeutet, Spaß zu haben. Tanzt, lacht und freut Euch über das Leben

## Schenken: Kostenlos oder zum Selbstkostenpreis

![Himmel mit Vögeln](/images/manual/mountains-1920x450.jpg?itok=5LX2f7_o "Himmel mit Vögeln")

**Alles im Leben hat seinen Preis. Ist das wirklich so?**

Jeder kommt in seinem Leben mal an einen Punkt, an dem er sich Gedanken darüber macht, was wirklich wichtig ist. Dazu zählen für gewöhnlich die Familie, Freunde, Gesundheit, Glück, Freiheit und Frieden. Ein Preisschild könnte niemals ausdrücken, wie wertvoll z.B. die Familie oder die Freiheit für jeden einzelnen von uns ist - wenn nicht sogar unbezahlbar. Aber nur unser Herz kann den wahren Wert dieser für uns so wichtigen „Dinge“ erkennen, der nicht in Zahlen zu ermessen ist. Liebe z.B. ist unbezahlbar und doch steht sie jedem Menschen in der einen oder anderen Form kostenlos zur Verfügung. Was steht uns noch alles kostenfrei zur Verfügung? Es sind Dinge, die für uns oft selbstverständlich sind, wie zum Beispiel Zeit, Sonne, Flora und Fauna, Luft, Wasser, Nähe zu anderen Menschen, Glaube und der freie Wille

Wenn also das, was uns wirklich wichtig ist, nicht mit Geld aufzuwiegen ist, dann sollten die Dinge, für die wir Geld benötigen doch mindestens in der Prioritätenliste nach hinten rutschen dürfen.  
Aus dieser Überlegung heraus folgt für uns Helfas die Überzeugung, dass wir uns alles, was im Leben Bedeutung hat und was wir benötigen, gegenseitig schenken können. Und für all das, was wir (noch) nicht selbst herstellen können, gibt es die Möglichkeit, Geld zu schenken oder den Selbstkostenpreis innerhalb der H.e.l.f.a.-Gemeinschaft zu bezahlen.

**Was** **bedeutet es zu schenken?**  
Zu Schenken heißt, jemandem eine Gabe zu dauerndem Besitz zu überlassen, um den anderen damit zu erfreuen, ohne dafür eine Gegenleistung zu verlangen. Und ist es nicht so, dass sich nicht nur die Empfänger über ein solches Geschenk freuen, sondern auch wir selbst? Von Herzen zu Schenken löst auf beiden Seiten ein intensives Gefühl der Freude und des Glücks aus. Ein Gefühl, das uns mit dem anderen verbindet.

Dies ist ein Grund, warum wir bei H.e.l.f.a. auch nicht Tauschen. Selbst im traditionellen Tauschhandel (Sachen gegen Sachen) kann es vorkommen, dass jemand, z. B. einen Schrank eintauschen möchte, den der Tauschpartner jedoch nicht benötigt. Was nun? Ferner ist es beim Tauschen üblich jegliche Objekte und Handlungen in bestimmte Werte einzukategorisieren und gegeneinander aufzuwiegen. Somit stellt auch der Tauschhandel von Gegenständen oder Dienstleistungen ein Ungleichgewicht und eine Benachteiligung dar, wenn die Bedürfnisse nicht übereinstimmen. Innerhalb des H.e.l.f.a.-Projekts schenken wir Frei, ohne Zwang und Erwartungen. Jede Hilfe und Unterstützung wird kostenlos oder maximal zum Selbstkostenpreis angeboten.

Uns ist bewusst, dass das Annehmen einer Gabe bei Manchem ein Gefühl der Schuld und des Unbehagens auslösen kann, mit dem gleichzeitigen Drang etwas als Dank oder Ausgleich zurückgeben zu müssen. Diese negativ behafteten Gefühle möchten wir bei H.e.l.f.a. überwinden und damit auch das „Annehmen können“ wieder neu erlernen. Denn jeder Mensch dieser Erde hat es verdient beschenkt zu werden.

Das, was geschenkt werden kann, ist so unglaublich vielfältig, dass eine Aufzählung dies hier nicht widerspiegeln kann. Beim Schenken geht es daher vor allem darum, ganzheitlich auf die Bedürfnisse der Menschen einzugehen.

**Was bedeutet der Selbstkostenpreis?**  
Im Falle von Reparaturen ist häufig nicht nur die Arbeitsleistung einer Person vonnöten, es werden auch mal Ersatzteile, z.B. für die Waschmaschine oder für das Auto benötigt. Hierbei fallen Kosten für die Beschaffung von Teilen an. Diese Ersatzteile kann der Geber ebenfalls verschenken. Ist dies aus bestimmten Gründen nicht möglich, trägt der Hilfesuchende die Kosten in Form des Selbstkostenpreises.  
Dies ist eines von vielen Beispielen, was wir bei H.e.l.f.a. unter dem Helfen zum Selbstkostenpreis verstehen. Das Übernehmen von Anfahrtskosten, um Hilfe in Anspruch nehmen zu können, kann auch darunter fallen. Wichtig ist, dass das Geld nicht im Vordergrund steht und immer wieder überlegt wird: Wie können wir das Helfen ohne Geld realisieren?

Auch Selbständige und Unternehmer können ihre Talente bei H.e.l.f.a. einbringen, indem sie Menschen mit ihren Fähigkeiten beschenken oder zum Selbstkostenpreis helfen. Der Vielfalt an Schenkungs-Ideen sind keine Grenzen gesetzt. Und damit wird das Profit-Denken deutlich reduziert.

**Gründe, weshalb es wichtig sein kann, den Selbstkostenpreis zu verlangen:**  
Ist ein H.e.l.f.a.-Mitglied selbständig, könnte das Schenken von zum Beispiel Ersatzteilen für ein Auto dazu führen, dass das Helfen mit einem finanziellen Verlust für den Helfer einhergeht. Das könnte bedeuten, dass der Selbständige wirtschaftlich leidet und dann wiederum nicht mehr helfen kann. Abgesehen davon, könnte es unter Umständen zu Schwierigkeiten mit dem alten System (Finanzamt) kommen.

Wir möchten, dass sich alle Menschen bei H.e.l.f.a. wohlfühlen und den Übergang vom altem System in eine Schenkungsgemeinschaft sanft, geschmeidig und im Sinne von Win-Win-Situationen für die Beteiligten gestalten.

Der Grundsatz, um den es immer wieder geht, ist, von Herzen gerne zu geben. Es soll kein Zwang entstehen, alles ist freiwillig und ohne Erwartungen. Nur so erreichen wir, dass wir die alten Gewohnheiten, wie den selbst erzeugten Druck und die Abhängigkeiten aus der alten Welt dauerhaft ablegen.

Je mehr Menschen aus dem alten System (nach und nach) aussteigen und mitmachen, desto eher wird sich die Vision einer Schenkungsgesellschaft selbst tragen können.

## Persönliches Kennenlernen/ Deanonymisieren

![Eine Kirche](/images/manual/church-1920x450.jpg?itok=zgx2SS97 "Eine Kirche")

Welchen Personen vertrauen wir unseren Wohnungsschlüssel an? Mit welchen Menschen würden wir über unsere tiefsten Sehnsüchte sprechen? Diese und andere Fragen sind Grundüberlegungen für H.e.l.f.a. wenn es um das Miteinander und das Schenken geht. Und es sind nur zwei von vielen Beispielen für die Vertrauen wichtig ist.

Nur Jemandem, dem wir zutiefst vertrauen, würden wir während unserer Abwesenheit den Zutritt zur Wohnung gestatten oder würden mit ihm über sehr private Gedanken reden.

Der momentan vorherrschende Zustand auf der Welt ist jedoch eine fortschreitende Entfremdung, die in Korrelation zur technologischen Entwicklung steht.  
Der technische Fortschritt bietet uns heute unglaubliche Möglichkeiten. Innerhalb weniger Sekunden können wir uns über diverse Plattformen mit Menschen am anderen Ende der Erde Vernetzen, Austauschen und weltweit Einkaufen ohne einem Anderen zu begegnen. Weiterhin ist das Internet eine immer weiter wachsende Quelle von Wissen, an dem wir mit nur wenigen Klicks teilhaben und uns weiterbilden können.

**Der Mensch in Nummern und Codes**  
Durch das Internet gewinnen wir aber nicht nur enorme Möglichkeiten, denn die damit verbundene Anonymität birgt auch Gefahren. Viele Menschen sind viel zu lang online und insbesondere die sozialen Medien haben sich massiv auf unser zwischenmenschliches Miteinander ausgewirkt. Eine lange Freundesliste auf Facebook, Instagram und Co. zu haben, ist keine Seltenheit, doch wissen wir wirklich wer sich hinter einem Benutzernamen oder einem Account verbirgt? Kann auf dieser Basis echtes Vertrauen entstehen?

Die mit der Digitalisierung immer weiter voranschreitende Anonymisierung von uns Menschen hin zu Codes und Nummern befeuert darüber hinaus nicht nur die massive Ausdehnung von Kontrolle und der damit verbundenen Bürokratie (der Mensch ist dann nur noch ein Datensatz) sie führt auch zu mehr Ungerechtigkeit, die auf diese Form der Entmenschlichung zurückzuführen ist. Das Arbeitsamt sieht beispielsweise nur noch seinen dokumentierten Schriftsatz und fällt auf Basis dessen Entscheidungen, um etwas an den Arbeitslosenzahlen oder den eigenen Erfolgsstatistiken zu verändern. Doch wo bleibt der Mensch? Und wem nützt die Anonymisierung? Die Antwort ist kurz: Sie nützt denen, die mit diesen Daten (deren Verkauf und deren Verarbeitung) Geld verdienen.

An all diesen Ausführungen erkennen wir, dass wir keine oder kaum Diskussionen darüber geführt haben, wie wir die Technik zum Besten des sozialen Miteinanders integrieren.

**Was wir besser machen wollen**  
Diese Überlegungen haben uns bei H.e.l.f.a. zu der Erkenntnis geführt, dass wir das Netz mit all seinen Vorteilen nutzen, die Nachteile jedoch, wann immer möglich, kompensieren wollen. Wir wollen das Internet und verschiedene Messenger als Kommunikationsplattform nutzen, gleichzeitig wissen wir, dass diese Werkzeuge den persönlichen Kontakt nicht ersetzen können, wenn es darum geht Vertrauen aufzubauen und zu festigen.

**Das persönliche Kennenlernen** steht daher bei H.e.l.f.a. an erster Stelle und dient neben dem Aufbau von Vertrauen auch der Sicherheit. Bei H.e.l.f.a. nennen wir das persönliche Kennenlernen „De_anonym_isieren“. Raus aus dem anonymen Internet und den sozialen Medien und rein ins reale Leben. Es braucht wieder ein vertrauensvolles Miteinander, damit der Schenkungsgedanke in der neuen Gesellschaft funktioniert.Das Schenken und das Kennenlernen sind demnach eng miteinander verknüpft und bedingen sich gegenseitig.

Im direkten Kontakt sehen wir den Menschen und nicht nur einen schnell verfassten Post, der das Menschliche dahinter schnell vergessen lässt. Uns zu kennen birgt eine völlig andere Grundlage, auf der wir uns mit Problemen und Konflikten auseinandersetzen. Wir geben uns mehr Mühe im Gespräch und Miteinander, um uns jederzeit in die Augen sehen zu können. Wertschätzung und Respekt bekommen dabei eine reale und authentische Bedeutung.

Und sich zu kennen bedeutet bei H.e.l.f.a. nicht nur sich namentlich zu kennen oder einmal gesehen zu haben, sondern sich regelmäßig zu treffen, gemeinsam zu kochen, Gemüse anzubauen, zu tanzen und sich generationenübergreifend zu helfen

Wenn persönliche Treffen mal nicht möglich sind, wie es beispielsweise in globalen [Gruppen](/node/24) der Fall ist, in denen die Mitglieder weit verstreut wohnen, können auch Telefonate und Online Video Meetings für die Kommunikation und den Gedankenaustausch genutzt werden.

**Gruppengröße**  
Der Mensch kann Studien zur Folge regelmäßig höchstens 150 - 200 Sozialkontakte pflegen. Dazu gehören alle Kontakte, die auf Vertrauen und gegenseitiger Unterstützung beruhen. Für ein weiteres Anwachsen der Gruppe über dieses typische Limit hinaus, reicht die Kapazität des Gehirns zur Verarbeitung sozialer Informationen nicht aus. Im Zuge eines weiteren Gruppenwachstums, wird demnach auf natürliche Weise eine neue Gruppe gegründet, um die sozialen Bande aufrecht erhalten zu können.

So handhaben wir die Gruppengrößen auch bei H.e.l.f.a. Um zu gewährleisten, dass wir uns tatsächlich alle kennen, sind die Gruppengrößen auf maximal 200 Mitglieder begrenzt. Sollte eine Gruppe diese Grenze überschreiten, wird eine weitere Gruppe aufgebaut.. Dabei ist jede [Gruppe](/node/28) autonom aktiv..

**Was bedeutet Vertrauen für H.e.l.f.a.?**  
Die Grundlage für Vertrauen wird mit dem ersten Kontakt geschaffen. Tiefes und stabiles Vertrauen aufzubauen braucht jedoch Zeit, häufigere Begegnungen, Gespräche und Erfahrungen. Es braucht einen Verzicht von Kontrolle, Macht und Egomanie und es braucht einen geschützten Rahmen, der individuelle Handlungsmöglichkeiten bietet, an denen wir wachsen können. Je besser wir uns kennenlernen, desto stärker wird das Vertrauen in Andere aber auch in uns selbst und unsere Entscheidungen.

Die Lernaufgabe für unser neues Miteinander lautet, wieder verlässliche Aussagen und Entscheidungen zu treffen und Integrität im Reden und Handeln zu leben. Dies fängt bereits damit an Terminzusagen einzuhalten oder, falls etwas dazwischen kommt, möglichst frühzeitig abzusagen. Dies drückt nicht nur Respekt vor der Zeit des Anderen aus, es ist auch ein Ausdruck der eigenen Verlässlichkeit.

Vor diesem Hintergrund können wir mit dem Aufrechnen von Dienstleistungen und Wertgegenständen aufhören, denn stabiles Vertrauen in unsere Mitmenschen ist die beste Währung und Versicherung, die es gibt. Wenn wir uns vertrauen, beginnen wir nach anderen Gesichtspunkten zu entscheiden, wie und mit wem wir Ideen umsetzen und zusammenarbeiten wollen.

Lernen wir uns also alle echt und wahrhaftig neu kennen mit all unseren individuellen Facetten, Eigenheiten und Potenzialen. Entwickeln wir wieder einen Bezug zueinander, um gemeinsam Projekte umzusetzen und beständige Freundschaften zu schließen.

Und lernen wir wieder Vertrauen in uns selbst und unsere Wahrnehmung zu haben.

###### Psychological view

**Was bedeutet Vertrauen für H.e.l.f.a.?**

Vertrauen ist eine Mischung aus instinktivem und erlebtem Wissen und Nicht-Wissen. Sind uns immer alle relevanten Umstände unseres Handelns bekannt, ist Vertrauen nicht notwendig, denn wir Wissen. Fehlt uns dieses Wissen, können wir nicht Vertrauen. Jemandem zu Vertrauen geht also immer mit einem gewissen Risiko einher, denn wir könnten enttäuscht werden und persönliche negative Konsequenzen davontragen. Aber anders herum können wir genauso gut positiv überrascht werden. Dieses Risiko im menschlichen Miteinander ist aber für jeden von uns ein Teil des Lebens. Es ist unrealistisch sich vor Enttäuschungen zu schützen. Es würde einem permanenten Verharren in der eigenen Komfortzone gleichen, die sicherlich für eine Zeit lang ganz schön sein kann und bequem ist. Die andere Seite dieser Medaille bedeutet jedoch, sich vom Leben abzuschneiden und keine Erfahrungen mehr zu machen (gleichbedeutend mit Stillstand und Tod), die für unsere Weiterentwicklung wichtig sind oder die wir später als unvergessliche Momente und wertvolle Lektionen in Erinnerung behalten.

## Moderatoren

![Sonnenuntergang Gruppen](/images/manual/Groups-1920x450.jpg?itok=zAeTeXds "Sonnenuntergang Gruppen")

**Schnittstellen der Vernetzung**  
Wichtig für uns von H.e.l.f.a. ist, dass die Menschen in den Gruppen zusammengeführt werden, einen Umgang auf Augenhöhe pflegen und sich wohlfühlen, um gut miteinander arbeiten zu können, aber auch, um gemeinsam Spaß zu haben.

Neben dem Umgang untereinander ist auch die „Versorgung“ aller Lebensbereiche in den Gruppen ein wichtiger Aspekt. Doch was, wenn z.B. anwaltliche Hilfe, technische Unterstützung oder anderes Know-How benötigt wird, welches nicht über die [Lokalgruppe](/node/34) abgedeckt werden kann?

Für solche und andere Fälle gibt es innerhalb jeder Gruppe Ansprechpartner, die sich gruppenübergreifend um die Vernetzung und Kommunikation kümmern, aber auch immer wieder Impulse in Form von Seminaren u.ä. geben, sodass sich die Gruppe auch weiterentwickeln kann. Die Ansprechpartner können dann im Falle eines Hilfegesuches, das nicht durch die lokale Gruppe bedient werden kann, Verbindung zu Gruppen in anderen Regionen (regional, überregional, global) oder zu Dritten (Außenstehende) aufnehmen, die diese Ressourcen haben.

**Diese beschriebenen Ansprechpartner nennen wir bei H.e.l.f.a. Moderatoren.**

**Die Gruppe entscheidet, der Moderator moderiert**  
Um das Wir-Gefühl in den Gruppen sowie das Verantwortungsbewusstsein und die Selbstwirksamkeit der Mitglieder zu stärken, ist es wichtig wieder eine echte Basisdemokratie zu leben. Dieser Gedanke soll auch durch die Moderatoren gefördert werden. Während die eine Gruppe eben diese demokratische Struktur bevorzugt, benötigt eine andere Gruppe eine Art Leitung, um gut funktionieren zu können. Da dies ein Ungleichgewicht in der Gruppe mit sich bringt, soll eine solche Struktur die absolute Ausnahme vom demokratischen Standard sein. Uns von H.e.l.f.a. ist Augenhöhe und Gleichberechtigung sehr wichtig, um Machtgefällen vorzubeugen.

Das, was zählt, ist der Wille der gesamten Gruppe. Alle Entscheidungen, wie sich die Gruppe aufstellt und wohin sie sich entwickeln soll, werden daher auch von der Gruppe getroffen und nicht vom Moderator – Jeder kann mitentscheiden. Der Moderator begleitet den Prozess zur Entscheidungsfindung, indem er die Gruppe z.B. mit der aktiven Moderation und dem Einstellen von Umfragen unterstützt.

**Aufgabenbereiche der Moderatoren**  
Moderatoren sind gewählte Begleiter, die die Gruppe unterstützen und zusammenhalten und die den Grundstein für eine gleichberechtigte Kultur im Miteinander legen.

**Die Moderatoren einer Gruppe ergänzen sich mit den folgenden Eigenschaften:**

- Etwas mehr Zeit haben
- Empathisch + Einfühlsam
- Verständnis von H.e.l.f.a. haben
- Lernbereitschaft
- Demütig
- Verständnis für Wir-Gefühl
- Technisches Verständnis
- Verantwortungsbewusst
- Organisationstalent

**Übersicht der Aufgaben:**

- Kontaktperson und Repräsentant für die eigene und für fremde Gruppen, wie die Orga, [Themengruppen](/node/29), usw.
- Sie kümmern sich um die Belange der Gruppe:

  - Initiator, Organisator, Koordinator wenn Feste anstehen
  - Vermittler, Informant, wenn etwas gebraucht wird, ob dies nun materiell ist oder Informationen → Hierbei kann sich der Moderator auch Unterstützung aus der Gruppe holen, sodass er nicht alle anfallenden Aufgaben selbst bearbeitet sondern diese auf mehrere Schultern verteilen kann.

- Fürsorge für Mitglieder – achten auf Erholung und Life-Balance, sodass die Gruppenmitglieder nicht zu viel arbeiten und dabei ihre Gesundheit aufs Spiel setzen
- Achten auf eine „anständige“ Kommunikation (Moderation) innerhalb der Gruppe (egal ob im Netz oder in der Realität) - hier kann eine sog. Nettikette helfen

  - Fungieren gegebenenfalls als Mediator für die Gruppenmitglieder oder finden andere Lösungen bei Konflikten

- Website-Präsentation der Gruppe einstellen und pflegen
- Einführung neuer Mitglieder aus dem Empfang in die Gruppe – er ist Kontaktperson und sollte von allen gekannt werden

**Übersicht zu Unterstützungsmöglichkeiten für Moderatoren:**

- Weitere Moderatoren in der eigenen Gruppe
- Moderatoren-Gruppen
- Orga
- Seminare
- Regionale und überregionale Treffen

H.e.l.f.a. empfiehlt für jede Gruppe mehrere Moderatoren (5% bezogen auf die Gruppengröße), die sich die oben aufgelisteten Aufgaben teilen. Dies soll auch dem Schutz der Moderatoren vor Überlastung dienen, aber auch einer Zentralisierung von Macht entgegenwirken. Zudem hat jeder Mensch besondere Fähigkeiten, wodurch sich mehrere Moderatoren gegenseitig viel besser ergänzen und unterstützen können.

Damit die Moderatoren ein Bewusstsein für die Verantwortung als Begleiter einer Gruppe bekommen und sich gestärkt und sicher ihren Aufgaben widmen können, bietet H.e.l.f.a. Seminare und Schulungen an. Diese sind für **alle** Mitglieder zugänglich und sollen dabei unterstützen die eigenen Kompetenzen zu erweitern. Zukünftige Moderatoren können hier lernen, wie man Gruppen begleitet und auch wie mit Konflikten umgegangen werden kann. Auf diese Weise lernen, wachsen und entwickeln wir uns gemeinsam weiter.

Durch die Schulungen kann jede Gruppe ihre Handlungskompetenz erweitern und herausfinden, wie sie am Besten arbeiten kann. Nur durch ein breites Wissen und eine Vielfalt an Methoden kann ein H.e.l.f.a. sicher entscheiden, wie er in der jeweiligen Situation am Besten handelt und sich damit gut fühlt.

Aktuell wird darauf hingearbeitet, regionale Orgas zu bilden, die den einzelnen Gruppen und Moderatoren vor Ort noch besser helfen können. Die Orgas werden daher immer nur dann in Gruppenbelange eingreifen, wenn sie darum gebeten werden. Wir von H.e.l.f.a. haben Vertrauen in die Mitglieder und in ihr Bewusstsein darüber, dass jeder im Grunde genau weiß, was er sich zutraut, was er braucht und was das Beste für ihn ist.

**Dezentralisierung**
Der H.e.l.f.a.-Gedanke steht für ein gleichberechtigtes Miteinanderleben in einer dezentralen und autonomen Gesellschaft. Eine Anhäufung von Macht, dessen destruktive Resultate wir zugenüge aus dem aktuellen System kennen, widerspricht diesem Gedanken, daher wurden einige Maßnahmen getroffen, um einer Machtkonzentration entgegenzuwirken:

- Mehrere Moderatoren in der Gruppe
- Jährliche Moderatoren-Wahl
- Begrenzung der Moderatoren-Amtszeit auf 1 Jahr

Zusätzlich darf ein Mitglied nur in einer Gruppenform Moderator sein. So kann der Moderator einer Lokalgruppe nur in dieser Lokalgruppe Moderator sein und darf nicht auch noch in den umliegenden Lokalgruppen Moderator werden.

Wir sind alle nichtsdestotrotz Menschen, sodass es trotzdem vorkommen kann, dass Moderatoren ihre Position zu missbrauchen, zum Beispiel indem versucht wird, Leute aufgrund von Gerüchten oder Antipathie auszuschließen auf Grund von Gerüchten oder Sympathie / Antipathie .

**Sollte ein Moderator versuchen seine Stellung bewusst oder unbewusst auszunutzen, kann und soll sich die Gruppe wehren.**

Zunächst ist es hilfreich das Gespräch mit dem Moderator zu suchen und ihn auf sein Verhalten hinzuweisen. Sollte dies zu keiner Veränderung des Verhaltens beim Moderator führen, kann die Gruppe ein **Misstrauensvotum** einleiten, wodurch der Moderator gezwungen ist, sich und sein Verhalten sowie seine Entscheidungen zu erklären. Der dadurch angestoßene Klärungsprozess kann dazu beitragen die zugrundeliegenden Beweggründe der vom Moderator getätigten Handlungen zutage zu fördern, z.B. ob zum Wohle und zum Schutz der Gruppe gehandelt wurde oder aus einem Ego-Bewusstsein heraus. Sollte die Gruppe in einem solchen Gespräch erkennen, dass nicht zum Wohle der Gruppe gehandelt wurde und das Verhalten nicht toleriert werden kann, kann eine Abwahl eingeleitet werden.

Wie das Verfahren für ein Misstrauensvotum genau abläuft, darf und soll die autonome Gruppe selbst entscheiden. **Das Vorgehen beim Einleiten eines Misstrauensvotums gilt dabei nicht nur für Moderatoren sondern für alle Mitglieder, die sich nicht an die untereinander vereinbarten Gruppenregeln halten.**

Sollte sich die Gruppe unsicher fühlen oder eine Schieflage zu spät erkennen, können sich die Mitglieder auch an einen anderen Moderator in ihrer oder in einer anderen Gruppe wenden. Zudem gibt es bei H.e.l.f.a. die sogenannten „Orgas“, die als Ansprechpartner für mehrere Gruppen dienen. Hierzu gehört unter anderem die Deutschland Orga, die ebenfalls dazu da ist H.e.l.f.a.-Mitglieder zu unterstützen und zum Beispiel eine Mediation anzubieten.

**Wir von H.e.l.f.a. wünschen uns, dass viele Mitglieder aktiv werden** und uns als Moderator dabei unterstützen, gemeinsam eine humane und helfende Gesellschaft aufzubauen. Eine Gesellschaft in der wir wieder mitentscheiden können, liebevoll miteinander umgehen und auch kleine zwischenmenschliche Begegnung und Gesten wieder wertschätzen.

Helft uns dabei, Menschen wieder näher zusammenzubringen und unsere Unabhängigkeit durch die gegenseitige Unterstützung zu fördern.

## Regeln

![Brennender Himmel](/images/manual/sunset-1920x450-3.jpg?itok=La5uTfVR "Brennender Himmel")

In jedem Land auf dieser Welt gibt so unglaublich viele Gesetze, Verordnungen und Normen, dass nicht einmal Richter und Anwälte sie alle kennen. Noch dazu lassen diese Gesetze so viel Interpretationsspielraum, dass die Rechtsprechung häufig vom Gesetzes-Verständnis des jeweiligen Richters abhängt. Wie soll ein normaler Mensch diese Masse an Gesetzen und deren Tragweite, die zudem noch unglaublich kompliziert verfasst wurden und sich ständig ändern, jemals vollständig erfassen und verstehen? Wer sich aus persönlichen Gründen einmal näher mit den einzelnen Rechtsbereichen auseinandergesetzt hat, stellt nicht selten fest, dass viele der darin enthaltenen Gesetze uns eher einschränken statt dienen und das nicht immer nur, um andere Menschen und ihren Lebensraum zu schützen. Führen wir diese Kausalitätskette weiter, so stellen sich noch viele weitere Fragen, wie u.a. ...

- Wem dienen diese vielen Gesetze?
- Warum sind sie so kompliziert verfasst?
- Warum gelten sinnlose Verordnungen aus dem letzten Jahrhundert noch heute und werden nicht überprüft und verworfen?
- Wie frei sind wir Menschen?

**Regeln sollen den Menschen dienen**  
Wir von H.e.l.f.a. sind der Ansicht, dass grundsätzlich jede Regel den Menschen dienen sollte und nicht umgekehrt. Die Regeln sollten außerdem für alle Menschen gelten und keine Privilegien oder Übervorteilung für bestimmte Gesellschaftsschichten oder Menschengruppen bedeuten. Wir wünschen uns eine friedliche Gesellschaft, in der alle Menschen gleichberechtigt sind und mit ihren Fähigkeiten und Talenten zum Wohle der Gemeinschaft beitragen.

- Die Freiheit des Einen bedingt immer auch die Freiheit des Anderen. Oder anders formuliert: Was du nicht willst, das man dir tu, das füg auch keinem Anderen zu.
- Die vierte Regel beinhaltet daher wie wir mit Regeln umgehen wollen und wie diese erstellt und beschlossen werden.

**So einfach und klar wie möglich**  
Es gibt so viele verschiedene Menschen auf der Welt und so viele regionale und auch kulturelle Unterschiede und Gegebenheiten, sodass zusätzlich zu den H.e.l.f.a. Regeln jede Gruppe ihre eigenen Regeln definieren kann. Dies birgt die Möglichkeit eben diese Unterschiede in der Erstellung der Regeln zu berücksichtigen und damit die Grundlage zu schaffen, dass sich die Mitglieder zugehörig und wohl fühlen.

Damit alle Gruppenmitglieder wissen, wie Regeln erstellt werden und weshalb sie erstellt wurden, gibt es die folgenden **Grundsätze**:

Die Regeln werden am besten so einfach, klar und übersichtlich wie möglich formuliert, sodass jeder sie verstehen kann. Um die Verständlichkeit jeder Regel zu garantieren, soll bei jeder Regel ein Positiv- und ein Negativbeispiel angeführt werden.

**Dies bedeutet:**

- der Grund, weshalb die Regel aufgestellt wird, muss ausformuliert werden und
- es muss dargestellt werden, was mit der Regel _**nicht**_ gemeint ist oder erreicht werden soll.

**Eine Regel am Beispiel zum Umgang** **mit** **einem Misstrauensvotum, könnte wie folgt aussehen:** „Sofern sich ein Mitglied durch einen Moderator ungerecht behandelt fühlt, kann sich dieses Mitglied an einen anderen Moderator/ ein anderes Gruppenmitglied als Vertrauensperson wenden. Gemeinsam besprechen sie die Situation und sprechen mit dem Moderator. Ein Misstrauensvotum soll erst nach zwei erfolglosen Gesprächen aller Beteiligten ausgesprochen/angeregt werden. Damit kann die Gruppe erreichen, dass Kommunikation und Diskurs ihre Berechtigung und ihren Raum erhalten, um mögliche Missverständnisse gemeinsam und wertschätzend zu klären. Außerdem soll damit erreicht werden, dass keine vorschnellen Urteile gefällt werden und Menschen eine zweite Chance erhalten.“

Die Regeln orientieren sich damit immer am Thema oder Problem für das sie gedacht sind und es wird auch erklärt, warum die Gruppe die Regel braucht und aufstellt. Die Mitglieder sind damit angehalten, sich mit der Regel zu beschäftigen und sie so zu formulieren, dass sie jeder verstehen kann. Das macht die Regeln auf der einen Seite ausführlich und auf der anderen Seite einfach und nachvollziehbar.

**Damit die Anzahl der Regeln in einem überschaubaren Rahmen bleibt, gibt es die folgenden Hilfestellungen zur Erarbeitung von Regeln:**

- Jede Regel hat ein Start- und ein Enddatum. Nach dem Enddatum wird innerhalb einer Gruppendiskussion mit gemeinsamer Abstimmung noch einmal zwingend darüber nachgedacht, ob die Regel in der Form noch sinnvoll ist und gebraucht wird.

Wenn nein...

… wird die Regel ersatzlos gestrichen.

Wenn ja…

… wird die Regel ggf. aktualisiert und ein neues Enddatum festgesetzt.

**Warum Regeln?**

Je mehr Menschen zu einer Gruppe gehören, desto sinnvoller können verbindliche Regeln werden, die zum Beispiel eine Orientierung für den gemeinsamen Umgang untereinander geben. Dies ist vor allem dann hilfreich, wenn sehr unterschiedliche Charaktere aufeinandertreffen und die Gefahr besteht, dass die Interesse und/oder das Verhalten des Einen zulasten der Rechte eines Anderen gehen.

Im allgemeinen sind Regeln dafür da, um ein friedliches Miteinander zu gewährleisten, in der die Rechte aller Mitglieder gewahrt und geachtet werden. Wie dieses Miteinander im Einzelnen aussehen soll, das kann jede Gruppe eben individuell / autonom entscheiden. Und von H.e.l.f.a. ist wichtig, dass wir uns wieder respektvoll und in Liebe begegnen und dies in den Gruppen auch leben. Im besten Falle spiegelt sich dieser Grundgedanke in den selbst aufgestellten Regeln wider.

## 10 Prinzipien, die hier vertreten werden

![Mohnblumen](/images/manual/poppies-1920x450.jpg?itok=fEOKcEnr "Mohnblumen")

Zusätzlich zu den 4 Regeln gibt es 10 Grundsätze, die wir hier vertretn, um eine Orientierung zu geben.

## 1\. Bestrebung der Menschheit

Das höchste Bestreben der Menschheit ist das Gemeinwohl aller lebenden Arten und der Biosphäre.  
Menschen, Tiere und Pflanzen gehören alle zur Natur und können nicht von ihr getrennt werden. Wir sind Teil der Natur - nicht außerhalb von ihr oder über ihr. Alle Arten sind miteinander verbunden und für ihr Überleben indirekt voneinander abhängig.  
Der physische Einfluss des Menschen auf die Welt ist heute so groß, dass wir uns weise und verantwortungsvoll verhalten müssen. Wir müssen bei allen Entscheidungen sowohl die Bedürfnisse aller Arten und der Umwelt als auch unsere eigenen Bedürfnisse berücksichtigen.

## 2\. Das Leben – kostbar und frei

Das Leben ist in all seinen Formen kostbar und frei, um sich im Sinne des Gemeinwohls zu entfalten.

Das Leben ist ein erstaunliches Phänomen und bis jetzt ist unser Planet der einzige, auf dem wir es gefunden haben. Dies macht es noch ungewöhnlicher und schöner, und deswegen sollte es wertgeschätzt und respektiert werden.

Wie auch immer, Leben bedeutet auch Nahrung zu sich nehmen zu müssen, und jede Spezies dieser Welt ist Teil einer komplexen Nahrungskette. Um diese Nahrungskette aufrecht zu erhalten und - damit auch die Biodiversität und das Überleben - müssen wir das Gemeinwohl aller Spezies in unser Denken mit einbeziehen und entscheiden, wie wir miteinander umgehen.

## 3\. Ressourcen

Die natürlichen Ressourcen der Erde stehen all ihren Bewohnern von Geburt an zur Verfügung und es steht uns frei diese für das Gemeinwohl zu nutzen.

Jede lebende Person und Kreatur auf diesem Planeten ist automatisch dazu berechtigt alle natürlichen Ressourcen der Erde zu teilen und diese zu benutzen, um ein gesundes und erfülltes Leben ohne Schuld oder Unterordnung unter eine andere Person oder Kreatur, leben zu können.

Die Menschheit hat aufgrund ihrer Populationsgröße und ihres komplexen Lebensstils eine besondere Pflicht, diese natürlichen Ressourcen nicht zu überlasten oder mehr davon zu benutzen als nötig ist, um im Sinne des Gemeinwohls weiter ein glückliches und vollwertiges Leben zu führen.

## 4\. Gleichberechtigung

Jeder Mensch ist in der weltweiten Gemeinschaft der Menschen gleichberechtigt und ein freier Bürger dieser Erde.

Soziale, ideologische und geographische Grenzen zwischen den Leuten, sind menschengemachte Barrieren, die einer physischen oder natürlichen Grundlage entbehren. Solch künstliche Trennungen stehen dem Gemeinwohl und dem Überleben der gesamten Gesellschaft entgegen.

Unsere Gemeinsamkeiten sind hingegen sowohl physisch als auch natürlich. Um es allgemeiner auszudrücken, wir alle brauchen und wollen die gleichen Dinge. Durch eine allgemeine Zusammenarbeit und gleichberechtigten Zugang können wir unsere Fähigkeiten effektiver anwenden um diese gemeinsamen Bedürfnisse zu befriedigen.

## 5\. Zusammenarbeit mit allem

Unsere Gemeinschaft beruht auf Zusammenarbeit und einem Verständnis der Natur, welches wir durch unsere Grundausbildung vermittelt bekommen.

Jedes Kind, dem eine nützliche und relevante Bildung über das Funktionieren der Natur, der Welt und das Leben in der Gemeinschaft vermittelt wird, wird dieser Welt und dieser Gemeinschaft letztendlich das Beste geben. Die traditionelle karrierebezogene Ausbildung hat sich in eine messbar zerstörerische Richtung entwickelt.

Letztendlich wird die Bildung die Regulierungen ersetzen, welche im Grunde genommen nur ein grobes System darstellen, um die Ordnung aufrecht zu erhalten. Zum Beispiel ist ein Kind besser für das Leben gerüstet, wenn es vollkommen versteht, warum eine bestimmte Tat nicht möglich ist, als ein Kind, das nur die Angst kennt, für diese Tat bestraft zu werden.

## 6\. Grundlagen für's Leben

Unsere Gemeinschaft versorgt alle ihre Mitglieder mit den Grundlagen für ein gesundes, erfülltes und nachhaltiges Leben, frei und ohne Schuldigkeit.

Jeder Person sollte der technisch höchstmögliche Lebensstandard zur Verfügung gestellt werden, ohne dabei Geld, Handel oder Schulden zu benutzen oder aufzuerlegen. Es gibt keinen logischen Grund mehr dies zu tun. Jedes menschliche Leid wird praktisch durch unser veraltetes Tauschsystem verursacht.

Jede Form von Schuld und Unterordnung ist nicht nur hinderlich sondern inzwischen auch vollkommen unnötig. Das ist auf unsere technologische Entwicklung und die einfache Art und Weise mit der wir Güter für uns herstellen und bauen können, zurückzuführen.

## 7\. Grenzen der Natur

Unsere Gemeinschaft respektiert die Grenzen der Natur und ihre Ressourcen, wobei sie den Verbrauch und die Abfallproduktion so gering wie möglich hält.

Unsere Ressourcen sind begrenzt weil es sie nur in begrenztem Maße gibt oder weil es einige Zeit in Anspruch nimmt sie zu verwalten und zu ersetzen. In beiden Fällen müssen wir unsere Ressourcen schonen, um unsere Versorgung sicherzustellen und um die Umwelt zu schützen.

Zusätzlich müssen wir das Sammeln von Gegenständen, die wir kaum benutzen, reduzieren. Denn diese haben eine direkte Auswirkung auf unsere Umwelt. Je mehr wir unsere Umwelt erhalten, desto größer sind unsere Überlebenschancen.

## 8\. Lösungen von uns

Die Lösungen, die unsere Gemeinschaft vorschlägt, beruhen auf Logik und dem besten zur Verfügung stehenden Wissen.

In einer neuen Gesellschaft ohne finanzielle Hindernisse und Beschränkungen, wird die größte Herausforderung für die Menschheit eine technische sein. Z.B.: wie versorgen wir alle mit ausreichend Lebensmitteln, Wasser, Unterkunft, Energie, Materialien und stellen gleichzeitig einen hohen, nachhaltigen Lebensstandard für alle sicher?

Im Gegensatz zur traditionellen Politik und der Spekulation, ist die wissenschaftliche Methode ein nachweisbares, robustes System um diese technischen Probleme unter der Anwendung der verfügbaren Fakten und Logik, zu lösen. Die wissenschaftliche Methode verfügt zudem über einen gemeinsamen kultur- und sprachenübergreifenden Bezugspunkt.

## 9\. Versorgungspflicht und Mitgefühlspflicht

Unsere Gemeinschaft erkennt ihre Versorgungspflicht und Mitgefühlspflicht gegenüber Mitgliedern an, die nicht in der Lage sind, etwas dazu beizutragen.

Menschen, die aus irgendeinem Grund nicht dazu in der Lage sind, sich um sich selbst zu kümmern oder etwas zum Gemeinwohl beizutragen, sollten von der Gemeinschaft räumlich, leiblich und seelisch versorgt werden ohne damit eine Schuldigkeit auf sich zu ziehen.

Es ist auch unerlässlich, dass wir unseren Kindern, als zukünftigen Mitwirkenden unserer Gemeinschaft, so viel nützliches Wissen wie möglich vermitteln um ihre Kreativität, ihr Wachstum und ihren Intellekt im Sinne der zukünftigen Weiterentwicklung zu stimulieren.

## 10\. Verantwortung

![Sonnenuntergang Gruppen](/images/manual/Groups-1920x450.jpg?itok=zAeTeXds "Sonnenuntergang Gruppen")

Unsere Gemeinschaft erkennt ihre Verantwortung, einen vielfältigen und nachhaltigen Lebensraum für die zukünftigen Generationen zu erhalten, an.

Wir müssen daran denken, dass wir unseren Planeten nicht nur mit anderen Menschen, Tieren und Pflanzen teilen, sondern dass er auch Ursprung der zukünftigen Menschen, Tiere und Pflanzen sein wird.

Diese Lebewesen, die sich heute nicht äußern können und keinen Einfluss auf unser Tun haben, haben das gleiche Recht zu leben wie wir. Es ist in unser aller Interesse wenn wir den zukünftigen Generationen die Welt so hinterlassen wie wir sie vorgefunden haben, wenn nicht in einem besseren Zustand.

## Wie fange ich an?

## Erster Schritt

Der erste Schritt, um in die Helfa Welt zu kommen ist der einstieg in eine Lokalgruppe. Dort lernt sich jeder kennen und baut Vertrauen auf. In der Realität ist das sehr einfach, in der Informatik ist es etwas schwieriger. Dort kann jeder sich bei einem Moderator melden und sich da vorstellen bzw. einen Termin machen, um sich zu treffen. Wird anstatt die Website, Telegram benutzt, ist der erste Punkt eine Empfangsgruppe. Dort kann jeder sich dann bei einem Administrator vorstellen oder den Begrüßungstext durchlesen, der einem Anweisungen gibt, wie es weiter geht. Jedenfalls ist auch dort einer der ersten Schritte, dass der Moderator angeschrieben wird. Ist der erste Kontakt geschafft, ist der Rest nur noch ein Kinderspiel – denn ab jetzt steht einem die Helfa Welt zur Verfügung.

Das Treffen ist deshalb so wichtig, weil Vertrauen nur aufgebaut werden kann, wenn sich die Leute wirklich kennen. In der jetzigen Gesellschaft wird sow etas ähnliches gemacht: Dort haben alle einen Ausweis. Wir versuchen es ohne Ausweis zu machen. Wir versuchen es durch eine Gruppenbindung hinzubekommen, dass jeder jeden kennt. Denn wenn ich weiß, in welcher Gruppe jemand drin ist, kann ich den Moderator fragen, ob er ihn kennt und damit kann ich ihn identifizieren. Natürlich ist das nicht 100% sicher, aber nichts ist 100% sicher – doch geht es sehr weit und es gibt keine Datei oder Akte, die jemanden für sein ganzes Leben lang vorgehalten werden kann. Es werden nur die Informationen rausgegeben, die wichtig sind zu diesem Zeitpunkt.

###### Technical view

Test

## Zweiter Schritt

Wenn der Eintritt in die Gruppe erfolgt ist, ist jeder in der Lage, über die Website mehrere Informationen zu bekommen. So können sich die Themengruppen angesehen werden, die Berufsgruppen und auch die Organisationen (kurz Orgas). In jeder dieser Gruppen ist es möglich, sich anzumelden und sich dort zu integrieren, sich zu informieren und sich insgesamt auszutauschen. Es ist sogar erwünscht, dass Informationen ausgetauscht werden, denn nur dadurch kann jeder seinen Wissensstand erweitern und sich mit anderen Menschen verbinden. Aber, wie immer gilt auch hier, nicht in der virtuellen Welt verlieren, sondern das als Informationspool oder Kommunikationsnetzwerk nutzen, gelebt wird in der Realität. Also Infos schnappen und sie in die eigene Lokalgruppe umsetzen. Denn das Leben findet in der Realität statt – bitte nicht vergessen, egal wie verlockend das Internet ist.

Die nächsten Schritte sind so individuell, dass sie hier nicht aufgeführt werden können. Nur eins sollte jeder wissen:

Du bist frei zu tun, was Du magst. Lass Dich nicht einschränken. Aber pass auch bitte immer auf Deine Nachbarn auf – denn Du bist nicht allein. Nur zusammen schaffen wir das. Und wie sage ich immer:

**Frieden schaffen wir nur, wenn wir alle zusammenhalten. Also lasst uns alle zusammenhalten und Frieden schaffen – und lasst es uns in Liebe machen.**

## Definitionen

![Berge mit Nebel](/images/manual/mountain-1920x450-2.jpg?itok=IONFgzQd "Berge mit Nebel")

Auf den folgenden Seiten wird unsere Struktur beschrieben. Wann ist man Mitglied bei Helfa? Was sind die Aufgaben eines Moderators und so weiter. Schau es Euch an.

## Nachrichten Kategorien

![Blaue Schmetterlinge](/images/manual/fantasy-1920x450.jpg?itok=ISGvkStr "Blaue Schmetterlinge")

Information und Kommunikation sind wichtig für die Menschen und bringen sie zusammen. Aber die Informationsflut kann auch sehr anstrengend sein. Was ist wichtig? Was lese ich?  
Um Ihnen einen Überblick und eine Auswahl zu geben, haben wir derzeit eine Liste von Kategorien erstellt. So können Sie sehen, was Sie interessiert und die Informationen besser filtern. Damit es auch optisch unterstützt, haben wir jeder Kategorie eine Farbe zugewiesen.  
Helfa ist da, wo Sie sind. Nach diesem Motto freuen wir uns auf Ihre Anregungen. Welche Kategorie vermissen Sie? Was können wir verbessern? Und natürlich freuen wir uns auf Ihre aktive Mitarbeit.

###### Technical view

###### Farbkategorien

| Kategorie     | Farbe    |
| ------------- | -------- |
| Nachrichten   | Grau     |
| Technik       | Blau     |
| Organisation  | Rot      |
| Politik       | Schwarz  |
| Kultur        | Braun    |
| Natur         | Grün     |
| Gesellschaft  | Orange   |
| Gesundheit    | Gelb     |
| Spiritualität | Violett  |
| Hilfe         | Hellblau |

## Mitglieder

![Glückliche Familie](/images/manual/Group5-1920x450.jpg?itok=yBnhv-xD "Glückliche Familie")

Jeder Mensch hat das Recht und auch die Möglichkeit nach den 10 Prinzipien zu leben. Jeder Mensch darf sich demnach als H.e.l.f.a. bezeichnen. Nur hat sich in der Vergangenheit gezeigt, dass einige Menschen noch nicht so weit sind und dass sie das Konzept versuchen auszunutzen.

Um einem Ausnützen vorzubeugen, organisieren wir uns in Gruppen und versuchen dadurch zu erreichen, dass sich die Gruppen selbst schützen. Damit es gelingt, dass Gruppen geschützt sind, muss jeder Einzelne, der einer H.e.l.f.a. Gruppe zum Beispiel über Telegram beitritt und bei H.e.l.f.a. mitmachen möchte, sich immer auch real bei einer H.e.l.f.a. Gruppe vorstellen oder muss zumindest ein Seminar besuchen, damit ihm alle Regeln wie auch die Vision von H.e.l.f.a. erklärt werden kann.

Damit kann sichergestellt werden, dass jeder weiß, was innerhalb der Regeln erlaubt ist, was getan werden darf und was nicht, ist das persönliches Kennenlernen und das Vertrauen in eine Gruppe aufzubauen unerlässlich, um eine neue Art von Gesellschaft zu kreieren. [Dies haben wir in der zweiten Grundregel von H.e.l.f.a. beschrieben](/node/10)

**Die Empfangsgruppen von H.e.l.f.a.**

H.e.l.f.a. organisiert sich unter anderem auf Telegram in Gruppen. Diese Gruppen dienen der Vernetzung und dem Austausch. Jeder Interessent kann auf Telegram (t.m/Helfa_Projekt) eine Empfangsgruppe in seinem Bundesland finden. Auch über die Homepage ([www.helfa.org](http://www.helfa.org/)) können die Menschen eine Gruppe in ihrer Nähe / ihrem Ort finden und dem Empfang beitreten.

In den sogenannten Empfangsgruppen können die Interessenten einen Admin / Moderator anschreiben oder werden von den H.e.l.f.a.s zu einem Kennenlerntreffen eingeladen. Nach mindestens einem Treffen werden die neuen Mitglieder in die Haupt-Lokalgruppe eingeladen, in der sich ausschließlich H.e.l.f.a. Mitglieder befinden. In dieser H.e.l.f.a. Gruppe tauschen sich die Mitglieder aus, vereinbaren Treffen, um sich noch besser kennen zu lernen und Projekte zur Gestaltung einer neuen Gemeinschaft zu initiieren.

**Die Aufgaben der Mitglieder sind:**

- Die 4 Grundregeln lernen und verstehen
- Sich mit den 10 Prinzipien auseinander setzen
- Sich für die Gruppe Zeit nehmen, die anderen H.e.l.f.a.s kennenlernen und sich in die Grupp integrieren.
- Offen sein für Neues und sich auf der Webseite über aktuelle Themen informieren
- Einmal im Jahr einen Moderator wählen
- Die Regeln der eigenen Gruppe befolgen und mit abzustimmen, wenn Abstimmungen anstehen. Jedes Mitglied sollte sich also aktiv mit seinen Talenten und Fähigkeiten einbringen

###### Technical view

Wichtig sind dabei die folgenden Daten, die wir bevorzugt elektronisch auf der Homepage verarbeiten möchten:

Pflichtfelder

| Feld               | Erklärung                                                |
| ------------------ | -------------------------------------------------------- |
| E-Mail             | Wird für die Anmeldung verwendet                         |
| Benutzername       | Ist der Benutzername im System                           |
| Name, Vorname      | Ist der echte Name, der verwendet wird                   |
| Lokale Gruppe      | In welcher lokalen Gruppe ist das Mitglied               |
| Kontakt Öffentlich | Ein öffentlicher Kontakt, der auf der Website erscheint. |
| Privater Kontakt   | Ein Kontakt, den nur die eigene Gruppe sehen kann.       |
| Sprachen           | Welche Sprachen spricht die Person                       |
| Beschreibung       | Eigene Beschreibung, Aussehen, Verhalten, etc.           |
| Rolle              | Welche Rolle hat das Mitglied? Moderator?                |

Mögliche Felder

| Feld        | Erklärung                                                                 |
| ----------- | ------------------------------------------------------------------------- |
| Bilder      | Welche unterstreichen die Beschreibung des Mitglieds.                     |
| Wünsche     | Welche Wünsche hat das Mitglied - ggf. können diese erfüllt werden.       |
| Ressourcen  | Welche Ressourcen hat das Mitglied zur Verfügung                          |
| Fähigkeiten | Diese sind wichtig, um zu wissen, was das Mitglied kann                   |
| Adresse     | Diese Adresse kann dann in einer Karte angezeigt werden                   |
| Geschlecht  | Dieser Eintrag ist wichtig für eine Zuordnung oder persönliche Vorlieben. |
| Geburtstag  | Dieser Eintrag ist wichtig für Altersbeschränkungen                       |

## Moderatoren

Moderatoren sind Mitglieder, die eine bestimmte Gruppe vertreten. Sie sind weder die Chefs der Gruppen, noch sind sie besser oder schlechter als alle anderen. Ihre Aufgabe ist es, die Gruppe zu leiten, zu verstehen und für sie ansprechbar zu sein. Die Moderatoren sind die erste Anlaufstelle, wenn es darum geht, etwas über die Gruppe zu erfahren, ihr zu helfen oder sie anderweitig zu kontaktieren. Dies gilt sowohl für die eigenen Mitglieder der Gruppe als auch für andere Gruppen von außerhalb. Um es einem Moderator leichter zu machen, gibt es spezielle Gruppen, in denen Moderatoren Informationen austauschen und sich gegenseitig helfen können.  
Moderatoren gibt es in verschiedenen Gruppen, und natürlich haben sie alle die gleichen Rechte und können daher gemeinsam Informationen austauschen. Damit die Machtbegrenzung funktioniert, darf ein Mitglied jedoch nur in einer Art von Gruppe Moderator sein. Der Moderator einer lokalen Gruppe kann also nur in einer lokalen Gruppe Moderator sein und darf nicht in den umliegenden lokalen Gruppen Moderator sein. Das Gleiche gilt für lokale Themengruppen, Sprachgruppen und Berufsgruppen.

Aufgaben des Moderators:

- Ansprechpartner und Vertreter für die eigene Gruppe und andere wie Orga, Themengruppen.
- Wird für ein Jahr gewählt, vom 1.1. - 31.12.
- Öffentliche Kontaktdaten (z.B. E-Mail) sind Pflicht (auch wenn es nur eine Mailadresse ist, damit man die Helfa-Kommunikation machen kann)
- Verantwortlich für die Gruppe - kann aber auch Aufgaben delegieren
- Neue Mitglieder in die Gruppe einführen
- Aktualisierung der Website der Gruppe
- Verwalten von übergeordneten Gruppen, falls gewünscht.
- Unterstützung anderer Moderatoren in der eigenen Gruppe
- Spezielle Unterstützung durch verschiedene Moderatorengruppen
- Spezielle Unterstützung durch die Orga
- Erweitertes Benutzerhandbuch für Moderatoren
- Seminare für Moderatoren
- Regionale und nationale Treffen

###### Technical view

In diesem Zusammenhang sind folgende Daten wichtig, die wir am besten elektronisch verarbeiten sollten:  
Keine Angaben - siehe Mitglieder.

## Botschafter

Botschafter sind Mitglieder, die zu den Lokalgruppen fahren und dort Seminare vor Ort geben oder den Gruppen helfen und sie dabei unterstützen, Probleme zu lösen oder die Gruppenaktivität zu fördern. Botschafter können da helfen, wo sie gebraucht werden. So kann eine Gruppe, die einer anderen helfen möchte, Botschafter senden, die Informationen und Hilfe direkt vor Ort anbieten.

**Aufgaben bzw. Fähigkeiten des Botschafters:**

- Reisebereitschaft
- Viel Hintergrundwissen von H.e.l.f.a.
- Gut mit Menschen umgehen können

Folgende Daten sind dabei wichtig, die wir elektronisch verarbeiten würden:

Keine Besonderheiten – siehe Mitglieder.

###### Technical view

In diesem Zusammenhang sind folgende Daten wichtig, die wir am besten elektronisch verarbeiten sollten:  
Keine Angaben - siehe Mitglieder.

## Gruppen

Es gibt diverse Gruppenarten in H.e.l.f.a., die alle miteinander zu tun haben. Innerhalb der Gruppen gibt es verschiedene Ebenen, die eine Art Hierarchie abbilden. Diese Hierarchie dient **NICHT** dem versuch, die „unteren“ Gruppen in irgendeine Richtung zu manövrieren, sondern sie dient **AUSSCHLIESSLICH** der Informationsweitergabe innerhalb der Gruppenstruktur und der Koordination. Durch diese Form der Struktur und Informationsweitergabe wird zum Beispiel ein überregionales Treffen oder eine Abstimmung ebenso möglich, wie der Austausch darüber, wer bei einer Katastrophe oder in einer Notsituation welche Rolle übernimmt.

### Aufteilung in Ebenen

Es gibt verschiedene Ebenen, die sich an den realen Örtlichkeiten ausrichten. Nur zwei Ebenen sind absolut, und damit sehr gut definierbar:

- Lokalgruppen
- Globalgruppen

**Die Gruppengröße ([kurze Wiederholung](/node/10))**

Bei H.e.l.f.a. haben wir die Gruppengröße der Lokalgruppen auf höchstens 200 Mitglieder je Gruppe begrenzt. Erkenntnisse aus der Psychologie und Soziologie zeigen, dass ein Mensch ca. 150 andere Menschen persönlich gut kennen kann. Und selbst bei dieser Größe wird es schon schwierig den Überblick zu behalten. Die Erfahrung aus den bestehenden Gruppen zeigt, dass eine Anzahl zwischen 80 und 120 Mitgliedern optimal ist.

**Autonomie der Gruppen**

Die jeweiligen regionalen H.e.l.f.a.- Gruppen sind autonom. Sie können selbst innerhalb der Wachstumsgrenze entscheiden, wie groß sie werden oder welche Projekte sie umsetzen wollen. Durch regionale oder kulturelle Gegebenheiten und Unterschiede ist es für uns wichtig, dass jede Gruppe diese Unterschiede in ihren Entscheidungen individuell einfließen lassen kann.

Zusätzlich möchten wir betonen, dass weder die Orgas noch die [Moderatoren](/node/11) oder gar Kal, der Initiator von H.e.l.f.a., einer Gruppe vorschreiben können, was sie abgesehen von den [4+1 Regeln](/node/73) und den [10 Prinzipien](/node/13) von „Die freie Welt Charta“ ([_https://freeworldcharter.org/en_](https://freeworldcharter.org/en)_)_ zu tun oder zu lassen hat.

Ein weiterer Vorteil der Autonomie ist, dass durch diese regionale und autonome Herangehensweise eine Netz-Struktur entsteht. Innerhalb dieser deutschlandweiten und hoffentlich auch irgendwann globalen Netz-Struktur bildet jede Gruppe einen eigenen Knoten / Kernpunkt, der gleichzeitig zum Gesamtgebilde beiträgt und dieses stabilisiert. Diese Netz-Struktur hat die Vorteile einer lokalen Überschaubarkeit und bietet gleichzeitig eine globale Solidarität.

**Funktion der Gruppenarten**

Die beiden Gruppenarten beschreiben die Gruppen, die entweder in der Hierarchie ganz unten stehen (Lokalgruppen) oder weltumfassend sind (Globalgruppen).

Die unterste Ebene ist diejenige, wo jeder jeden kennen sollte oder sogar muss. Deshalb ist und bleibt dieser Gruppentyp relativ klein mit max. 200 Menschen, um den Kontakt zueinander halten zu können.

Die globale Ebene brauchen wir, damit wir alle Gruppen gesamt ansprechen können. Denn die Menschen sind nicht wirklich getrennt voneinander zu betrachten, nur weil das menschliche Vermögen dazu nicht ausreicht. Alle Menschen haben alle das selbe Recht, sich zu informieren, zu organisieren und sich mit ihren Talenten einzubringen. Dieses Recht ist bitte nicht damit zu verwechseln, dass jeder gleich behandelt werden soll. Außerdem, warum sollte ein Mensch, der woanders geboren wurde, anders behandelt werden? Nein, wir bieten mit unseren Gruppentypen eine Struktur, in der jeder Mensch mit jedem anderen Menschen frei kommunizieren kann – egal wo auf der Welt wir uns befinden.

**Zwischen diesen beiden absoluten Gruppenebenen gibt es zwei weitere Gruppenebenen, die variabel erstellt und genutzt werden können:**

- Regionalgruppen
- Überregionalgruppen

Die Regionalgruppen beinhalten die jeweiligen Lokalgruppen und können noch ein oder zwei Ebenen darüber sein. Wie die Struktur in einem Bundesland oder auch einem Kontinent sinnvoll ist oder sein kann, das entscheiden die Gruppen bzw. wird sich dies in der Praxis zeigen. Beispiele haben wir unten aufgeführt. Dadurch, dass wir bei H.e.l.f.a. autonome Gruppen und ganz gezielt keine Machtstrukturen sondern Flexibilität aufbauen wollen, werden wir auch hier viel ausprobieren und erfahren dürfen.

Die Überregionalgruppen beinhalten Regionalgruppen und können auch mehrere Ebenen darstellen. Wie viele hängt immer von der Anzahl der Ebenen ab.

**Beispiele**:

Erde → Kontinent → Land → Bundesland → Region → Stadt → Stadtteil → Gruppenname

In diesem Fall wäre die Erde die Globalgruppe, der Gruppenname die Lokalgruppe. Von Region bis Stadtteil wären das die Regionalgruppen und vom Kontinent bis zum Bundesland die Überregionalgruppen.

Erde → Kontinent → Land → Stadt → Gruppenname

Hier wäre wieder die Erde die Globalgruppe, der Gruppenname die Lokalgruppe. Die Stadt wäre dann die Regionalgruppe und der Kontinent und das Land die überregionalen Gruppen.

## Aufteilung in Ebenen

Es gibt verschiedene Ebenen, die sich an den realen Örtlichkeiten ausrichten. Nur zwei Ebenen sind absolut, und damit sehr gut definierbar:

- Lokalgruppen
- Globalgruppen

Diese beiden Gruppenarten beschreiben die Gruppen, die entweder in der Hierarchie ganz unten sind (Lokalgruppen) oder Weltumfassend (Globalgruppen). Die unterste Ebene ist diejenige, wo es persönlich ist, wo jeder jeden kennt. Die muss relativ klein sein, weil die menschliche Auffassungsgabe einfach nicht mehr kann – Psychologen gehen davon aus, dass man 120 bis 150 Menschen persönlich kennenlernen und Kontakt halten kann. Alle weiteren Menschen führen zu abstrakten Gruppen. Die globale Ebene brauchen wir, damit wir alle Gruppen ansprechen können. Denn die Menschen sind nicht wirklich getrennt voneinander zu betrachten, nur weil das menschliche Vermögen dazu nicht reicht. Die Menschen sind die Menschen und sie haben alle das selbe Recht – bitte nicht zu verwechseln damit, dass jeder gleich behandelt werden soll. Außerdem, warum sollte ein Mensch, der woanders geboren wurde, anders behandelt werden? Nein, wir bieten eine Struktur, wo jeder Mensch mit jedem anderen Menschen frei kommunizieren kann – egal wo wir sind.

Zwischen diesen beiden absoluten Gruppenebenen gibt es zwei weitere Gruppenebenen, die variabel benutzt werden können:

- Regionalgruppen
- Überregionalgruppen

Die Regionalgruppen beinhalten die Lokalgruppen und können noch ein oder zwei Ebenen darüber sein. Die Überregionalgruppen beinhalten Regionalgruppen und können auch mehrere Ebenen darstellen. Wie viele hängt immer von der Anzahl der Ebenen ab.

Beispiel:

Erde → Kontinent → Land → Bundesland → Region → Stadt → Stadtteil → Gruppenname

In diesem Fall wäre die Erde die Globalgruppe, der Gruppenname die Lokalgruppe. Von Region bis Stadtteil wären das die Regionalgruppen und vom Kontinent bis zum Bundesland die Überregionalgruppen.

Erde → Kontinent → Land → Stadt → Gruppenname

Hier wäre wieder die Erde die Globalgruppe, der Gruppenname die Lokalgruppe. Die Stadt wäre dann die Regionalgruppe und der Kontinent und das Land die Überregionale Gruppe.

## Lokalgruppe

Lokale Ortsgruppen sind die Gruppen, die ganz am unteren Ende der Hierarchie sind. Sie können Städte, Stadtteile oder Straßen sein, je nachdem wie dicht das Netzwerk in der Gegend ist. Diese Gruppen dienen zum Eingang ins Helfa Netzwerk, denn nur wenn wir uns kennen, können wir Vertrauen aufbauen. Und Vertrauen ist das, was wir brauchen, um etwas zu verändern.

Aufgaben der Gruppe:

- Sich um die eigenen Mitglieder kümmern
- Sich mit den Mitglieder von anderen Gruppen vernetzen
- Projekte auf die Beine zu stellen, um die Gruppe auch in Notsituationen aufrecht zu erhalten
- Immer mehr anbieten, bis das Geld nicht mehr so wichtig ist
- Spaßfaktor nicht vergessen
- Eine Zukunft für sich und die Kinder aufbauen

###### Technical view

Folgende Daten sind dabei wichtig, die wir am Besten elektronisch verarbeiten würden:

###### Muss Felder

|                        |                                                                                                |
| ---------------------- | ---------------------------------------------------------------------------------------------- |
| Status                 | wird weiter unten erklärt                                                                      |
| Logo                   | Jede Gruppe kann sich ein Logo geben                                                           |
| Beschreibung           | Jede Gruppe kann sich selbst beschreiben                                                       |
| Bilder / Bildergalerie | Es können mehrere Bilder in die Gruppe hochgeladen werden                                      |
| Termine                | Die Termine werden in einem Kalender in der jeweiligen Gruppe angezeigt                        |
| Sprachen               | In welcher Sprache / welchen Sprachen wird in der Gruppe gesprochen                            |
| Warnungen              | Meldungen, die angezeigt werden, wenn etwas passiert                                           |
| Empfangsgruppen        | Links, die auf die Gruppen verweisen, egal in welchem System                                   |
| Moderatoren            | Die Mitglieder, die kontaktiert werden können, damit neue Mitglieder aufgenommen werden können |

###### Kann Felder

|          |                                                                                                                                                                                                                                                 |
| -------- | ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| Regeln   | Dies sind Regeln, die jede Gruppe selbst erstellen kann                                                                                                                                                                                         |
| Artikel  | Dies sind Berichte, die jede Gruppe selbst verfassen kann                                                                                                                                                                                       |
| Anfragen | Hier können Sie Anfragen eingeben, um die sich die Gruppe kümmern soll                                                                                                                                                                          |
| Gruppen  | Hier können Sie den Link eingeben, der zu den Gruppen führt. Eine externe Gruppe, der andere Personen beitreten können, eine interne Gruppe. In beiden Gruppen sollten die Leute drin sein, damit sie merken, wenn jemand von Helfa Hilfe sucht |

## Themengruppen

Da Helfa die Gesamtheit der Menschen und damit auch alles Interessante abdecken will, würde eine flache Hierarchie zu unübersichtlich werden. Themengruppen haben auch eine Hierarchie, damit sie besser gefunden werden können. So gibt es ein allgemeines Thema, das dann Unterthemen haben kann, die wiederum Unterthemen haben können, und so weiter.

Zum Beispiel hat das Thema Natur die Unterthemen Naturschutz, Wald und Bepflanzung. Bei der Bepflanzung könnten die Unterthemen Ackerland, Kleingärten, Parks ... usw. sein. Jedes Thema hat seinerseits verschiedene Ebenen, da sie in allen Gruppen gemacht werden können. Der einzige Zweck dieser Hierarchien besteht darin, die Informationen auf mehrere oder einzelne Gruppen zu verteilen, falls erforderlich. So werden Leute, die sich allgemein für die Natur interessieren, genauso über ein mögliches Treffen informiert wie Permakultur-Leute und solche, die sich für Parks interessieren (Themengruppenhierarchie alle Themen vom Thema Natur bis ganz nach unten, aber nur die lokalen Gruppen, die in der Nähe des Treffpunkts registriert sind). Andererseits kann ein Seminar, bei dem es nur um Permakultur geht, auf diese Gruppe beschränkt werden, während alle lokalen Gruppen global angesprochen werden (natürlich mit der gleichen Sprache)

## Berufsgruppen

Die Berufsgruppen sind im Endeffekt wie die Themengruppen, nur dass dort Mitglieder eingepflegt werden, die in einem bestimmten Berufszweig sind oder sich extrem gut in diesem auskennen. Als Strategie sollte jede Gruppe verschiedene Berufsgruppen in den Lokalgruppen haben, um verschiedene Situationen besser meistern zu können. So sollte jede Helfa Lokalgruppe mindestens einen Arzt haben, einen Gärtner, einen Mechaniker, einen Psychologen, einen Heiler usw. Dadurch kann jede Gruppe für sich selbst sorgen, ohne dass weite Strecken gefahren werden müssen. Was nichts bringt ist, wenn eine Gruppe viele Ärzte hat und sonst sehr wenig. Auch bringt es nichts, wenn eine Gruppe nur die Besten der Besten hat, die umliegenden Gruppen aber nichts. Erst eine Ausgewogenheit führt dazu, dass es den Gruppen gut geht.

Im überregionalen oder globalen Kontext können sich Berufsgruppen zusammenschließen und neue Organisationen erstellen. Sie können damit Konzepte aufbauen, Pläne und Entwürfe erstellen, die sie den Gruppen zur Verfügung stellen. So können beispielsweise verschiedene Arten von Konzepten für Schulen gemacht werden, wo jede Gruppe sich aus mehreren Vorschlägen das zusammensuchen kann, was sie braucht. Ärzte könnten eine eigene Ärztekammer aufmachen, um den Menschen nach anderen Standards zu helfen und sie zu heilen. Egal was passiert, diese Experten könnten Lokal, Regional, Überregional und Global der Menschheit helfen

Die Berufsgruppen können, müssen aber nicht, eine Vorgruppe haben – wir nennen die dann „Support von“. Diese Gruppe hat den Vorteil, dass die Expertengruppen nicht immer wieder gestört werden und sie sich innerhalb ihrer Gruppen mit Konzepten, Aufgabenstellungen oder das, was gerade ansteht, beschäftigen. In dem Support können die Mitglieder der Berufsgruppen sein, aber auch Leute, die die Gruppe supporten möchten – sie sollten allerdings ein bisschen Ahnung von der Materie haben.

## Sprachgruppen

Diese Gruppen sind spezielle Gruppen und unterscheiden sich von den anderen Gruppen, da sie keine Themen oder ähnliches beinhalten. Sie dienen nur einer weiteren Dimension, damit wirklich alle Menschen an dem Projekt teilnehmen können.

## Spezialgruppen

## Strategien

## Die 5 + 1 wichtigsten Eckpfeiler für das Überleben

Es gibt 5 wichtige Eckpfeiler, die wir zum Überleben brauchen. Diese sollten von den Helfas irgendwann einmal angeboten werden können, am besten völlig kostenlos.  
1\. Essen  
2\. Trinken  
3\. Unterkunft (inkl. Wärme oder Kälteresistenz)  
4\. Kommunikation  
5\. Mobilität wie Auto

Und natürlich  
1\. Spaß und Freude

## Dezentrale - Zentralisierung

Der Zentralismus hat den Vorteil, dass die Ressourcen optimal genutzt werden. Alles wird an einer Stelle entschieden, umgesetzt und erledigt. Wenn etwas geändert werden muss, geschieht das nur an einer Stelle und wird somit sofort umgesetzt. Der Nachteil ist, dass das Zentrum bedeutet, dass die Außenbereiche sehr weit entfernt sind - die Transportwege sind sehr lang und die Logistik muss daher sehr stark beansprucht werden. Kommt es zu einer Panne, sei es durch Fehlentscheidungen oder einen Abbruch der Arbeiten aus welchen Gründen auch immer, muss das ganze Projekt pausiert werden. Außerdem müssen alle Ausnahmen von allen örtlichen Gegebenheiten an dieser einen Stelle in der Zentrale berücksichtigt werden.

Dezentrale Systeme haben den Vorteil, dass sie sich an das lokale Umfeld anpassen, die Wege sehr kurz sind und beim Ausfall eines Systems die umliegenden Systeme einspringen können. Das Problem ist, dass es sich um redundante Systeme handelt, die jeweils ihre eigenen Ressourcen verbrauchen und zu Doppelarbeit führen.  
Wenn die beiden Systeme zusammenarbeiten und die Vorteile kombiniert werden, entstehen ausfallsichere Systeme, die weder viele Ressourcen verbrauchen noch lange Transportwege erfordern. Dazu ist es notwendig, dass Informationen schnell weitergegeben werden und die Zusammenarbeit sehr gut funktioniert. Im Gegensatz zu unserem heutigen System, in dem nur eigene Gruppen von dieser Art der Zusammenarbeit profitieren (Unternehmen), soll sie in Helfa in allen Bereichen stattfinden. Dies ermöglicht eine offene, nicht übermäßig redundante, funktionsübergreifende Zusammenarbeit zwischen verschiedenen oder sogar allen Gruppen.

Beispiele für diese Art von Arbeit wären:  
Helfa-Gruppen, Stromnetze, Notfallhilfe, Seminare, Lagerung von Waren, Informationsaustausch.

## Dezentralisierung - Zentralisierung anhand eines Beispiels

Wenn eine Information nur bei einem Mitglied liegt, gibt es ein Problem, wenn dieses Mitglied verhindert oder überlastet ist - denn dann ist die Information nicht mehr zugänglich. Um solche Fälle zu vermeiden, sollten die Informationen so schnell und effizient wie möglich verbreitet werden. Je mehr Personen über die Informationen verfügen, desto geringer ist die Wahrscheinlichkeit, dass sie verloren gehen. In der Informatik, aber auch in anderen Bereichen, hat sich gezeigt, dass die Ergebnisse besser werden, wenn mehr als eine Person an einer Sache arbeitet. Die Zusammenarbeit erhöht nicht nur die Effizienz und die Sicherheit, sondern auch den Engpass, der sonst einen Engpass verursacht.

Ein Beispiel: Ein Mitglied, das die Standorte von Gruppen in eine virtuelle Karte eintragen kann, hat als Einziger Zugang zu dieser Technologie, stößt bei 5.000 Anfragen an seine Grenzen und kann die Aufgabe nicht oder nur unzureichend erledigen. Wenn wir das aufteilen, können mehrere Personen gleichzeitig an der Aufgabe arbeiten. Wenn die 5.000 Personen ihre "Positionen" auf der Karte selbst bearbeiten, wird die Anfrage nicht nur schneller bearbeitet, sondern bei Änderungen oder Ergänzungen ist sichergestellt, dass dies sofort geschieht und alle "zufrieden" sind.

## Helfa und andere Organisationen mit Namen versehen

Die Namen der Projekte und Organisationen sind zwar schön und gut, haben aber ein Problem:

Sie sind Schubladen, die die Gesellschaft teilen.

Während die Lokalgruppen nur deshalb geteilt werden, damit sie eine kleine Größeneinheit bilden können, die den menschlichen Grenzen entsprechen, ist ein Name eines Vereins und sonstigen Organisationen eine Abgrenzung und führt früher oder später zu einer Spaltung im Denken und dann auch in der Realität. Der Name Helfa wird nur als ein Beispiel dafür genommen, um einen griffigen Begriff zu haben. Die Gruppen selber haben aber mit dem Namen nichts zu tun. So können auch andere Organisationen ihre Namen und Logos verwenden … solange niemand in irgendeiner Form gegen die grundlegenden Regeln und Prinzipien handelt.

Wir müssen irgendwann mal weg von diesem Schubladendenken, denn das führt dazu, das wir die Realität nicht mehr wahrnehmen. Wir stülpen einer Gruppe Eigenschaften über, die definitiv nicht auf jedes Mitglied zutreffen und damit tun wir den Menschen unrecht. Mit diesem Denken erleichtern wir anderen eine „Ad Hominem“, eine „Kein wahrer Schotte“ Argumentation, die nur darauf abzielt, diese Ungenauigkeit auszunutzen, um die eigene Weltansicht anderen aufzudrücken. Aus diesem Grund ist es wichtig, dass wir früher oder später genau das verlassen oder wir nehmen jeden Namen an, der irgendwie passend ist.

## OpenSource und Open Access Prinzip

In dem Helfa Projekt versuchen wir so viel von dem OpenSource oder Open Access Prinzip zu übernehmen, wie es möglich ist. Die Software, die eingesetzt wird, soll also OpenSource sein. Warum? Die Idee ist, dass wir zusammen transparent arbeiten. Das bedeutet aber auch, dass unsere Werkzeuge frei sein sollten – und transparent.

So werden Programme benutzt, wie:

- LibreOffice
- Drupal
- Linux
- Telegram
- Signal
- usw.

Das OpenSource Prinzip, bzw. das Open Access Prinzip endet nicht nur bei den Programmen, sondern geht auch in unsere Arbeitsweise mit rein. So werden Treffen protokolliert und das Protokoll wird dann ins Netz gestellt, damit auch andere sehen, was beschlossen wurde. Natürlich muss nicht immer alles ins Netz gestellt werden, aber die wichtigen Informationen sollten schon von allen gelesen werden – und wenn es nur wegen der Transparenz ist.

Weitere Daten, die ins Netz gehören sind Dokumente über Konzepte, Ideen, wissenschaftliche Arbeiten, Seminare usw. Je mehr Wissen geteilt wird, desto besser wird es für alle Menschen sein – denn Wissen darf nicht sterben! Wissen muss so weit reichen, wie es möglich ist. Deshalb unterstützen wir OpenSource und Open Access.

## Probleme und Lösungen

## Streitigkeiten in Gruppen

Es besteht die Möglichkeit, dass sich Mitglieder daneben benehmen und dass die Gruppe nicht weiß, wie mit den Unstimmigkeiten umgegangen werden soll. Nun, so etwas passiert immer wieder und ist ganz normal. In so einem Fall, sollte das Mitglied, um das es geht, von den Moderatoren oder einem bestimmten Mediator angesprochen werden und es wird darüber geredet, was passiert ist und wie das für die Zukunft vermieden werden kann. Welches Werkzeug zur Lösung dafür benutzt werden kann, ist jeder Gruppe selbst überlassen – wichtig ist nur, dass die Probleme aufgearbeitet werden, statt nach hinten zu schieben und es irgendwann nicht mehr reparabel ist.

Kommt es öfters zu Beschwerden und es kann nicht durch ein persönliches Gespräch geklärt werden, muss die Gruppe irgendwann darüber entscheiden, wie mit der Situation umgegangen werden soll. Dabei geht es nicht darum, dass jemand „fertig“ gemacht wird, sondern dass das Thema aufgearbeitet werden kann und alle eine Lösung finden. Der Moderator ist dabei nicht der Anführer, sondern eben nur ein Moderator, der für einen reibungslosen Ablauf sorgt. Dieses Verfahren nennen wir „Misstrauensvotum“, weil es irgendwann darauf hinausläuft, dass einem oder mehreren Mitgliedern nicht mehr getraut wird. In wieweit das „Treffen“ geht, bestimmen die Gruppen selbst. So kann es sein, dass die Gruppe sich von den einzelnen Mitgliedern trennt und eine Zusammenarbeit nicht möglich ist. Solange das Mitglied nicht gegen die Helfa Gemeinschaft vorgeht, kann sie aus einer Gruppe raus genommen werden, kann aber weiter in der Helfa Gemeinschaft dabei sein, indem sie in eine andere Gruppe geht. Schadet sie der Gemeinschaft, muss sie aus dem gesamten Projekt entfernt werden. Das bedeutet NICHT, dass diese Person nicht mehr die Helfa Idee leben kann – es bedeutet nur, dass sie erst einmal, aus Mangel an Alternativen, nicht mehr tragbar ist. Sollte sich die „Gesinnung“ ändern, ist es möglich, dass die Person irgendwann wieder zurückkommt. Denn wir gehen fest davon aus, dass wir Menschen alle zusammenhalten müssen, um Frieden zu schaffen. Und das geht nicht, indem einige Menschen von anderen separiert werden. Allerdings kann das nicht immer gemacht werden und zu jeder Zeit. Deshalb, wie geschrieben, ist eine erste Trennung manchmal unabdingbar.

Was gar nicht geht ist, dass Menschen, Mitglieder, ohne Vorwarnung und Grund einfach aus einer Gruppe entfernt werden. Das spricht gegen jedes der 10 Prinzipien. Deshalb lasst uns zusammen daran arbeiten, eine neue Art des Miteinanders zu lernen und es auch anzuwenden.

## Geldthemen

Natürlich ist Geld auch ein Thema bei Helfa – immerhin funktioniert die ganze Gesellschaft momentan damit. Auch bei dem Projekt ist es erlaubt über Geld zu reden, Menschen Tipps zu geben, wie sie es besser machen und wenn jemand Geld unbedingt braucht, darf es ihm geschenkt werden. Was wir aber bevorzugen ist, dass zuerst alle anderen Methoden ausprobiert werden sollten, bevor das Geld in den Vordergrund gerät. Wir haben schon als Kinder gelernt, dass es nur die Lösung Geld gibt und müssen wieder lernen, von diesen Ideen wegzukommen, um neue Wege beschreiten zu können. So kann ein Werkzeug anstatt neu gekauft zu werden eher von einem anderen Mitglied geschenkt oder zumindest geliehen werden. Das spart nicht nur Geld, sondern ist auch Recycling auf sehe hohem Niveau. Dabei steigt die Sozialkompetenz und die Sozialverantwortung, da wir uns wieder mehr und mehr mit unseren Mitmenschen auseinandersetzen. Anfangs werden wir Probleme damit haben, weil wir es gar nicht gewohnt sind, mit Menschen umzugehen. „Meine Bohrmaschine kenne ich. Ich weiß ja nicht, wie andere damit umgehen.“ Diese Sprüche und Ansichten bringen uns dazu, uns immer mehr zu isolieren. Der Hintergrund ist klar: Der Kapitalismus hat es geschafft, dass wir uns alles leisten können, zumindest ein Teil der Menschen, der größere Teil nicht,

Wir können in der momentanen Lage nicht ohne Geld leben. Dennoch sollten wir versuchen, Wege zu finden, die sich damit auseinandersetzen, wie wir es machen könnten. Denn wenn wir uns nicht die Zeit nehmen, es zu versuchen, werden wir es nie schaffen, vom Geld wegzukommen. Es geht zum Schluss gar nicht darum, ob wir mit Geld arbeiten oder nicht, sondern nur darum, ob wir es schaffen, endlich unsere gewohnten Muster aufzugeben, um unsere Zukunft und die unserer Kinder zu retten.

Ein erster Anfang ist, dass wir die Arbeit von dem Geld trennen. Der Grund ist sehr einfach:

Ein Mensch ist wertvoll, weil er ist. Und jeder Mensch soll das Recht haben zu leben. Mit dem aktuellen Geldsystem ist das nicht möglich, denn wer kein Geld hat, hat nicht das Recht zu leben. Das sehen wir bei den Menschen, die sich auf dieser Erde kein Essen leisten können und deshalb verhungern. Nicht die Arbeit macht uns zu Menschen, sondern das Leben. Das gilt nebenbei auch für alle anderen Lebewesen auf diesem Planeten, inklusive dem Planeten selbst, der ja unsere Heimat ist.

Ohne eigenes Geld verkommen diese Lebewesen aber zu Dingen, die wir kaufen können, mit denen wir handeln usw. Aus diesem Grund ist es wichtig, dass wir anfangen, das Leben und alle seine Möglichkeiten ohne Geld zu sehen und wert zu schätzen. Da die Gesellschaft es dennoch braucht, sind wir bei Helfa momentan noch gezwungen, auch dieses Mittel als eine Sache zu sehen und es zu benutzen. Dann aber nicht als lebenswichtiges Element, sondern nur eine Sache, die die Gesellschaft braucht. Deshalb ist es in Ordnung, wenn dann nicht nur über Geld gesprochen werden kann, sondern dieses auch verschenkt wird – allerdings nicht als Gegenleistung zu einer Arbeit, einem Gefallen oder sons teinem Tausch, sondern nur als Geschenk.

## Problemstart in Gruppen

Es gibt immer wieder Startschwierigkeiten in Gruppen, um entweder Moderatoren zu wählen oder dass die Gruppe anfängt, sich zu treffen. Hier ein Paar Ideen, wie die Gruppen anfangen können zu wachsen:

- Klassische Werbung machen, damit mehr Menschen in die Gruppe kommen und damit die Gruppe größer wird. Dadurch kommen auch mehr aktive Menschen in die Gruppe, die dann die Gruppe beleben können.
- Bei jedem Treffen die Menschen fragen, was sie sich wünschen und versuchen, diese Wünsche als lokale Themengruppe erstellen und versuchen, diese dann umzusetzen.
- Mehrere lokale Themengruppen erstellen, die die Mitglieder interessieren könnten
- Die Umgebung anschauen und versuchen, der Stadt, dem Dorf durch Aktionen zu verbessern, wie z.B. aufräumen der Straßen.
- Auf diversen Plattformen Sachen, die verkauft werden, einen speziellen Preis für Helfa Mitglieder machen – und das natürlich so auszeichnen. Sollten Nachfragen kommen, kann darüber aufgeklärt werden.
- Immer mal wieder um Hilfe fragen. Durch das Helfen bricht das Eis und es entsteht ein Kontakt.
- Die Gruppen, die keine Moderatoren wählen wollen, werden darauf hingewiesen, dass sie nur dadurch in den ganzen anderen Gruppen vertreten sein können. Das ist wichtig, so dass Informationen von und zu anderen Gruppen geschickt werden können.
- Entscheidungen werden so transparent gemacht wie möglich. Dadurch entsteht ein Gemeinschaftsgefühl
- Treffen mit anderen Gruppen sind immer wieder ein schönes Ereignis, machen Spaß und beleben jede Gruppe.
- Nicht nur arbeiten anbieten, sondern auch Spaßtage einplanen. So können Spielabende, Wellnesstage und gemeinsame Wanderungen das Wir-Gefühl stärken und dadurch die Gruppe neu beleben. Aktionen, die zusammenschweißen sind immer gut – anstatt nur „normale“ Treffen zu machen, die mit endlosen Diskussionen langweilig werden. Mehr in die Realität gehen, anstatt sich in Ideologien verfangen.

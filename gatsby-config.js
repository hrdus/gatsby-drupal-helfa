/**
 * Configure your Gatsby site with this file.
 *
 * See: https://www.gatsbyjs.com/docs/gatsby-config/
 */

const languages = ['de', 'fr', 'en'];
const defaultLanguage = 'de';

module.exports = {
  trailingSlash: "always",
  siteMetadata: {
    title: `H.e.l.f.a. Engagieren sich Leidenschaftlich Für Alle`,
    description: `Site with Gatsby frontend and Drupal backend`,
  },
  plugins: [
    `gatsby-plugin-postcss`,
    `gatsby-plugin-image`,
    `gatsby-transformer-sharp`,
    `gatsby-plugin-sharp`,
    {
      resolve: `gatsby-source-filesystem`,
      options: {
        name: `markdown-pages`,
        path: `${__dirname}/markdown-pages`,
      },
    },
    {
      resolve: `gatsby-transformer-remark`,
      options: {
        footnotes: true,
        gfm: true,
      },
    },
    {
      resolve: `gatsby-source-filesystem`,
      options: {
        path: `${__dirname}/locales`,
        name: `locale`
      }
    },
    {
      resolve: `gatsby-plugin-react-i18next`,
      options: {
        localeJsonSourceName: `locale`, // Refers to `gatsby-source-filesystem` result
        languages,
        defaultLanguage,
        fallbackLanguage: defaultLanguage,
        redirect: false,
        // if you are using Helmet, you must include siteUrl
        siteUrl: `https://next.helfa.org`,
        // if you are using trailingSlash gatsby config include it here, as well (the default is 'always')
        trailingSlash: 'always',
        // you can pass any i18next options
        i18nextOptions: {
          interpolation: {
            escapeValue: false // not needed for react as it escapes by default
          },
          keySeparator: false,
          nsSeparator: false,
          debug: false,
        },
        pages: [
          {
            matchPath: '/manual',
            excludeLanguages: ['fr', 'en']
          }
        ]
      }
    }
  ]
};

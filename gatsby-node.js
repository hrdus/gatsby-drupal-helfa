const path = require(`path`);

exports.onCreatePage = ({ page }) => {
  console.log("on create page: " + page.path);
};

exports.createPages = async ({ graphql, actions }) => {
  const { createPage } = actions;
  const markdownPageTemplate = path.resolve(`src/templates/markdown-page.js`);
  const result = await graphql(`
    query {
      allMarkdownRemark {
        nodes {
          id
          frontmatter {
            title
            slug
          }
        }
      }
    }
  `);
  result.data.allMarkdownRemark.nodes.forEach(node => {
    createPage({
      component: markdownPageTemplate,
      path: `${node.frontmatter.slug}`,
      context: {
        id: node.id,
        title: node.frontmatter.title,
      },
    });
  });
};

import React from "react";
import { graphql } from "gatsby";
import { Section } from "../components/ui";
import Layout from "../components/layout";
import Seo from "../components/seo";

export default function Template({
  data,
  location
}) {
  const { markdownRemark } = data;
  const siteTitle = data.site.siteMetadata.title;
  const { frontmatter, html } = markdownRemark;
  return (
    <Layout location={location} title={siteTitle}>
      <Seo title={frontmatter.title} />
      <Section backgroundColor="bg-helfa-light-green">
        <div className="text-left">
          <div
            className="markdown-page"
            dangerouslySetInnerHTML={{ __html: html }}
          />
        </div>
      </Section>
    </Layout>
  );
}

export const pageQuery = graphql`
  query($id: String!) {
    markdownRemark(id: { eq: $id }) {
      html
      tableOfContents(
        maxDepth: 3
      )
      frontmatter {
        slug
        title
      language
      category
      }
    }
    site {
      siteMetadata {
        title
        description
      }
    }
  }
`;
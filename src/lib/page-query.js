import { graphql } from 'gatsby';

export const pageQuery = graphql`
  query ($language: String!) {
    site {
      ...SiteMetadata
    }
    locales: allLocale(filter: {language: {eq: $language}}) {
      edges {
        node {
          ...I18n
        }
      }
    }
  }
`;

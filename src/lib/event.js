import {
  format as _format,
  formatDistanceToNow as _formatDistanceToNow,
  parseJSON,
} from "date-fns";
import { de } from "date-fns/locale";

export function findNextEventDate(dates) {
  const now = new Date();
  const nextFuture = dates.find(date => parseJSON(date.value) > now);
  return nextFuture || dates.slice(-1)[0];
}

export function findFutureEventDates(dates) {
  const now = new Date();
  const n = dates.findIndex(date => parseJSON(date.value) > now);
  return dates.slice(n);
}

export const format = date => {
  return _format(date, "cccc, dd. MMMM yyyy, HH:mm", { locale: de });
};

export const formatDistanceToNow = date => {
  return _formatDistanceToNow(date, { locale: de, addSuffix: true });
};

export const formatTime = date => {
  return _format(date, "HH:mm", { locale: de });
};

export const formatDayOfMonth = date => {
  return _format(date, "d.", { locale: de });
};

export const formatMonth = date => {
  return _format(date, "MMM", { locale: de });
};

export const formatShortDate = date => {
  return format(parseJSON(date), "dd.MM.yy");
};

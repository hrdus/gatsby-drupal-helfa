export const languageOptions = [
  { language: 'de', label: 'Deutsch (DE) - Deutschland' },
  { language: 'en', label: 'English (EN) - United Kingdom' },
  { language: 'fr', label: 'François (FR) - France' },
];

export const defaultLanguage = 'de';
export const languages = languageOptions.map(({ language }) => language);

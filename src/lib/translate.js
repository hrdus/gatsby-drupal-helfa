const de = {
  weekly: "wöchentlich",
  monthly: "monatlich",
  monthly_by_week_and_weekday: "monatlich (Woche und Tag)",
};

export const translateDE = key => {
  return de[key] || `Fehlende Übersetzung (${key})`;
};

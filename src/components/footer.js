import * as React from "react";
import { Link } from "gatsby";

import TelegramIcon from "../images/Telegram_Footer.svg";
import YouTubeIcon from "../images/YouTube_Footer.svg";
import { Footer } from "./ui";
import LanguageSwitcher from "./language-switcher";


const HelfaFooter = () => {
  return (
    <Footer backgroundColor="helfa-light-green">
      <div className="text-white flex justify-between flex-col md:flex-row">
        <div className="text-left mb-3">
          <LanguageSwitcher />
          <div>
            <ul className="list-none">
              <Link to="/contact">
                <li className="inline-block p-1">Kontakt</li>
              </Link>
              <Link to="/imprint">
                <li className="inline-block p-1 before:content-['|'] before:mr-2">
                  Impressum
                </li>
              </Link>
              <Link to="/privacy">
                <li className="inline-block p-1 before:content-['|'] before:mr-2">
                  Datenschutz
                </li>
              </Link>
            </ul>
          </div>
        </div>
        <div className="text-left md:text-right">
          <div className="py-2">
            <a href="https://t.me/Helfa_Projekt">
              <img
                src={TelegramIcon}
                className="inline-block w-8 mr-3"
                alt="Social Icon für Helfa auf Telegram"
              />
            </a>
            <a href="https://www.youtube.com/channel/UCt0yWUqrpIjshR3rbtEpDYg">
              <img
                src={YouTubeIcon}
                className="inline-block w-8"
                alt="Social Icon für Helfa auf Youtube"
              />
            </a>
          </div>
          <div>&copy; H.e.l.f.a.- {new Date().getFullYear()}</div>
        </div>
      </div>
    </Footer>
  );
};

export default HelfaFooter;


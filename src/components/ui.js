import React from "react";
import { Link } from "gatsby";
import clsx from "clsx";

import kalVideoThumbnail from "../images/Kal.jpg";
import playIcon from "../images/playBtn.svg";

export const VideoContainer = () => (
  <div className="w-[90%] mx-auto mb-4">
    <div className="relative">
      <img
        src={kalVideoThumbnail}
        className="w-full"
        alt="Video Interview mit Helfa Gründer Kal"
      />
      <div className="absolute top-1/2 left-1/2 -mt-7 -ml-7">
        <img src={playIcon} alt="Play Video Icon" />
      </div>
    </div>
  </div>
);

export const Section = ({
  backgroundColor,
  color,
  children,
  tag,
  ...props
}) => {
  const TagType = tag;
  const bgColor = backgroundColor ? `bg-${backgroundColor}` : "";
  const textColor = color ? `text-${color}` : "";
  return (
    <TagType className={clsx(bgColor, textColor)} {...props}>
      <div className="content mx-auto">
        <div className="p-6 text-center">{children}</div>
      </div>
    </TagType>
  );
};

Section.defaultProps = {
  tag: "section",
};

export const Footer = props => {
  return Section({ tag: "footer", ...props });
};

export const Button = ({ to, children, disabled, className }) => {
  const options = { to, disabled };

  return (
    <Link
      {...options}
      className={clsx(
        className,
        "rounded-full bg-helfa-light-green py-3 m-1 inline-block font-medium"
      )}
    >
      {children}
    </Link>
  );
};

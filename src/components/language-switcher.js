import { Link, useI18next, I18nextContext, useTranslation } from 'gatsby-plugin-react-i18next';
import React, { useState, useRef, useEffect } from 'react';
import clsx from 'clsx';

import LanguageIcon from "../images/Language.svg";
import { languageOptions } from '../lib/languages';
import CheckIcon from '../images/check.svg';
import ChevronDownIcon from '../images/chevron-down.svg';

const LanguageSwitcher = () => {
  const { t } = useTranslation('language-switcher');
  const context = React.useContext(I18nextContext);
  const { originalPath } = useI18next();
  const [showMenu, setShowMenu] = useState(false);
  const menuRef = useRef();
  const buttonRef = useRef();

  useEffect(() => {
    const handleOutsideClick = (event) => {
      if (!showMenu) {
        const show = buttonRef.current?.contains(event.target);
        setShowMenu(show);
      } else {
        const show = menuRef.current?.contains(event.target);
        setShowMenu(show);
      }
    };
    document.addEventListener('click', handleOutsideClick);
    return () => document.removeEventListener('click', handleOutsideClick);
  }, [showMenu]);

  return (
    <div className='relative'>
      <div className="py-2" ref={buttonRef}>
        <img
          src={LanguageIcon}
          className="inline-block h-7 mr-2"
          alt={t("Icon für Sprache")}
        />
        {languageOptions.find(({ language }) => language === context.language).label}
        <img src={ChevronDownIcon} className="inline-block" alt={t("Icon für klappbares Menü")} />
      </div>

      <ul
        ref={menuRef}
        className={clsx([
          (showMenu ? '' : 'hidden'),
          'absolute top-0 bg-white shadow-lg text-black rounded'
        ])}>
        {languageOptions.map((option, index) => (
          <li
            key={option.language}
            className={clsx("border-gray-200 p-2 border-b", { "rounded-t": index === 0, "rounded-b": index === languageOptions.length - 1 })}
            onClick={() => setShowMenu(false)}>
            <Link to={originalPath} language={option.language}>
              {option.label} {context.language === option.language &&
                <img src={CheckIcon} alt={t('Icon für Ausgewählt')}
                  className="inline-block w-5" />}
            </Link>
          </li>
        ))}
      </ul>
    </div >
  );
};

export default LanguageSwitcher;
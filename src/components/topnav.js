import { Link } from "gatsby";
import React, { useState } from "react";
import { CloseIcon, BurgerIcon } from "./icons";
import clsx from "clsx";

function MenuItem(props) {
  const { to, title, disabled } = props;
  return (
    <li>
      <Link
        to={to}
        className={`
          block py-2 pr-4 pl-3 text-white
          border-b border-helfa-dark-green
          hover:bg-gray-50
          ${disabled && "text-helfa-dark-green"}
      `}
      >
        {title}
      </Link>
    </li>
  );
}

function Menu({ menuShown }) {
  return (
    <div
      className={clsx(
        { hidden: !menuShown },
        `w-screen shrink-0 bg-helfa-light-green absolute top-16 left-0`
      )}
      id="mobile-menu"
    >
      <ul className={`flex flex-col`}>
        <MenuItem title="Anleitung" to="/manual" />
        {/* <MenuItem title="Gemeinschaft" to="#" disabled /> */}
        <MenuItem title="Veranstaltungen" to="/events" />
        {/* <MenuItem title="Galerien" to="/galleries" disabled /> */}
        <MenuItem title="Helfa News" to="/news" />
        {/* <MenuItem title="Sprache" to="/lang" disabled /> */}
      </ul>
    </div>
  );
}

function HamburgerButton({ toggleMenu, menuShown }) {
  return (
    <button
      data-collapse-toggle="mobile-menu"
      type="button"
      onClick={toggleMenu}
      className={`
            inline-flex items-center p-2 text-sm
            text-white rounded-lg
            hover:bg-helfa-dark-green
            focus:outline-none ring-inset
            focus:ring-2
            focus:ring-helfa-dark-green
        `}
      aria-controls="mobile-menu"
      aria-expanded={menuShown ? "true" : "false"}
    >
      <span className="sr-only">Open main menu</span>
      <BurgerIcon hidden={menuShown} />
      <CloseIcon hidden={!menuShown} />
    </button>
  );
}

function TopNav() {
  const [menuShown, setMenuShown] = useState(false);
  const toggleMenu = () => setMenuShown(!menuShown);

  return (
    <nav
      className={`bg-helfa-light-green text-white border-gray-200 px-2 sm:px-4 relative h-16`}
    >
      <div className="lg:container grid grid-cols-3  mx-auto h-full">
        <div className="justify-self-start self-center">
          <HamburgerButton {...{ toggleMenu, menuShown }} />
        </div>
        <Link to="/" className="self-center text-center">
          <span className="text-2xl font-bold inline-block">H.e.l.f.a.</span>
        </Link>
        <div className="">
          {/* Account button */}
          {/* Search button */}
        </div>
        <Menu {...{ menuShown }} />
      </div>
    </nav>
  );
}

export default TopNav;

import * as React from "react";

const ResponsiveDebug = () => {
  return (
    <div className="fixed top-0 left-0 bg-black opacity-25 text-sm text-white m-2 p-1 rounded z-50">
      <span className="visible sm:hidden">xs min-w &lt;640</span>
      <span className="hidden sm:inline-block md:hidden">sm min-w 640</span>
      <span className="hidden md:inline-block lg:hidden">md min-w 768</span>
      <span className="hidden lg:inline-block xl:hidden">lg min-w 1024</span>
      <span className="hidden xl:inline-block 2xl:hidden">xl min-w 1280</span>
      <span className="hidden 2xl:inline-block 3xl:hidden">2xl min-w 1536</span>
    </div>
  );
};

export default ResponsiveDebug;

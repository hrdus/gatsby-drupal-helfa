import React from "react";
import { formatDayOfMonth, formatMonth, formatTime } from "../lib/event";
import { translateDE } from "../lib/translate";
import { LocationIcon, VideoCameraIcon } from "./icons";
import { parseJSON, addMinutes } from "date-fns";

export const EventSummary = ({ event }) => {
  let {
    title,
    event_time,
    duration,
    is_recurring: isRecurring,
    recurring_schedule: recurringSchedule,
    event_type: eventType,
    event_link: eventLink,
  } = event;

  const eventTime = parseJSON(event_time);
  const hasPassed = eventTime < new Date();
  const eventEndTime = addMinutes(eventTime, duration);

  return (
    <div className="py-3 px-5">
      <div className="flex space-between">
        <div
          className={`
          bg-helfa-light-green py-2 px-4
          rounded shadow-lg
          w-20 h-16 text-center
          text-white font-semibold
          flex flex-col justify-center
          `}
        >
          <div>{formatDayOfMonth(eventTime)}</div>
          <div>{formatMonth(eventTime)}</div>
        </div>
        <div className="px-4 pt-1 w-full text-left">
          <h2 className="font-bold text-md">
            <span itemProp="headline">{title}</span>
          </h2>
          <div>
            <span className="text-sm">
              {formatTime(eventTime)}&mdash;{formatTime(eventEndTime)}
            </span>
            {isRecurring && <span className="text-sm">{" " + translateDE(recurringSchedule)}</span>}
            {hasPassed && <span className="italic text-gray-500 text-sm"> vergangen</span>
            }
          </div>
          <div>
            {eventType === `online` && (
              <span>
                <VideoCameraIcon className="h-4 w-4 inline-block text-gray-500" />{" "}
                <a
                  href={eventLink}
                  className="text-sm text-gray-500 underline align-middle"
                >
                  Link zur Veranstaltung
                </a>
              </span>
            )}
          </div>
        </div>
        <div
          className={`
          py-2 px-3
          w-16 h-16 text-center 
          text-black
          `}
        >
          {eventType === "presence" ? <LocationIcon /> : <VideoCameraIcon />}
        </div>
      </div>
    </div>
  );
};
